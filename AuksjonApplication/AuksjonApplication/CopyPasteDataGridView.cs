﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace AuksjonApplication
{
    class CopyPasteDataGridView : System.Windows.Forms.DataGridView
    {
        public ContextMenuStrip contextMenuStrip1;
        private IContainer components;
  
        public CopyPasteDataGridView()
        {
            this.InitializeComponent();
            //this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            this.AllowUserToResizeColumns = true;
            this.AllowUserToOrderColumns = true;
            this.AllowUserToResizeRows = true;
            //this.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
            this.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            this.RowHeadersVisible = true;
            //gjenstanderDgw.GridColor = System.Windows.SystemColors.ActiveBorder;
        }

        protected void PasteClipboard()
        {
            try
            {
                string s = Clipboard.GetText();
                string[] lines = s.Split('\n');
                int iRow = CurrentCell.RowIndex;
                int iCol = CurrentCell.ColumnIndex;
                DataGridViewCell oCell;

                foreach (string line in lines)
                {
                    if (iRow < RowCount && line.Length > 0)
                    {
                        string[] sCells = line.Split('\t');
                        int cellsLen = sCells.GetLength(0);
                        for (int i = 0; i < cellsLen; ++i)
                        {
                            if (iCol + i < ColumnCount)                     //Innenfor antall kolonner i gridet.
                            {
                                oCell = this[iCol + i, iRow];               //Target
                                if (oCell.ReadOnly)                         //Kan ikke skrive til read-only celler
                                {
                                    MessageBox.Show("Kolonne " + (iCol+i) + " er skrivebeskyttet");
                                    return;
                                }
                                else
                                {
                                    try
                                    {
                                        //if (oCell.Value.ToString() != sCells[i])
                                        {
                                            oCell.Value = Convert.ChangeType(sCells[i], oCell.ValueType);
                                            //oCell.Style.BackColor = Color.LightGray;
                                        }
                                        //else
                                        //    iFail++;
                                        //only traps a fail if the data has changed 
                                        //and you are pasting into a read only cell
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show("Feil under paste operasjon. Velg eksisterende rader for overskriving\n" + ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        ++iRow;
                    }
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("The data you pasted is in the wrong format for the cell");
                return;
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if ((keyData == (Keys.Shift | Keys.Insert)) || (keyData == (Keys.Control | Keys.V)))
            {
                PasteClipboard();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        public override bool BeginEdit(bool selectAll)
        {
            return base.BeginEdit(selectAll);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mouse_down);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }


        private void mouse_down(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ToolStripMenuItem toolStripMenuItemPaste = new ToolStripMenuItem();
                toolStripMenuItemPaste.Click += new EventHandler(toolStripItemPaste);
                toolStripMenuItemPaste.Text = "Paste";

                ToolStripMenuItem toolStripMenuItemExport = new ToolStripMenuItem();
                toolStripMenuItemExport.Click += new EventHandler(toolStripItemExport);
                toolStripMenuItemExport.Text = "Export";

                contextMenuStrip1 = new ContextMenuStrip();
                contextMenuStrip1.Items.Add(toolStripMenuItemPaste);
                contextMenuStrip1.Items.Add(toolStripMenuItemExport);

                System.Drawing.Point mpos = e.Location;
                System.Drawing.Point menu_pos = this.PointToClient(Cursor.Position);
                contextMenuStrip1.Show(this, menu_pos);
            }
            
        }

        private void toolStripItemCopy(object sender, EventArgs args)
        {
            //MessageBox.Show("Copy");
        }

        private void toolStripItemPaste(object sender, EventArgs args)
        {
            PasteClipboard();
        }

        private void toolStripItemExport(object sender, EventArgs args)
        {
            string worksheet_name = "nrhf_aukjonsliste";
            ExcelExtension.ExportToExcel(this, worksheet_name);
        }
    } 

    static class ExcelExtension
    {
        public static void ExportToExcel(this DataGridView dataGridView, string worksheetName)
        {
            string file_name = "";

            if (dataGridView == null)
            {
                throw new ArgumentNullException("ExportToExcel");
            }

            var excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook book = null;
            Microsoft.Office.Interop.Excel.Worksheet sheet = null;
            try
            {
                book = excel.Workbooks.Add();
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.FileName = book.FullName;
                sfd.Filter = "Excel arbeidsbok (*.xlsx)|*.xlsx|Alle filer (*.*)|*.*";

                if (DialogResult.Cancel == sfd.ShowDialog())
                {
                    Marshal.ReleaseComObject(book);
                    Marshal.ReleaseComObject(excel);
                    return;
                }
                file_name = sfd.FileName;

                sheet = (Microsoft.Office.Interop.Excel.Worksheet)book.Sheets.Add();
                sheet.Name = worksheetName;

                for (int i = 0; i < dataGridView.Columns.Count; i++)
                {
                    sheet.Cells[1, i + 1] = dataGridView.Columns[i].Name;
                }
                //sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[1, dataGridView.Columns.Count]).Font.Bold = true;
                for (int row = 0; row < dataGridView.Rows.Count; row++)
                {
                    for (int column = 0; column < dataGridView.Columns.Count; column++)
                    {
                        sheet.Cells[row + 2, column + 1] = dataGridView.Rows[row].Cells[column].Value;
                    }
                }

                book.SaveAs(file_name);
                book.Close();
            }
            finally
            {
                excel.Workbooks.Close();
                excel.Quit();
                Marshal.ReleaseComObject(sheet);
                Marshal.ReleaseComObject(book);
                Marshal.ReleaseComObject(excel);
            }
        }
    }
}
