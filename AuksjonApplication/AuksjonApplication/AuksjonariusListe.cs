﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonApplication
{
    public partial class AuksjonariusListe : Form
    {
        public AuksjonDataClassesDataContext ctx = new AuksjonDataClassesDataContext();
        public IEnumerable<auk_list> auksjonarius_liste;
        public int current_auksjon_id;

        public AuksjonariusListe()
        {
            current_auksjon_id = ctx.app_config_datas.Single().current_auksjon_id;
            InitializeComponent();
        }

        private void AuksjonariusListe_Load(object sender, EventArgs e)
        {
            auksjonarius_liste = from g in ctx.gjenstands
                                 where g.auksjon_id == current_auksjon_id
                                 orderby(g.utrop_nummer)
                                 select new auk_list
                                 {
                                     gjenstand_id = g.ID,
                                     utrop_nr = g.utrop_nummer ?? 0,
                                     navn = g.navn,
                                     vurdering = g.vurdering,
                                     selger = g.selger_nummer.ToString(),
                                     minstepris = g.minstepris.ToString()

                                     //bud1 = g.forhaandsbuds.
                                     //   Where(f => f.gjenstand_id == g.gjenstand_id).
                                     //   ElementAtOrDefault(0.0).pris,
                                     //   //Select(f => f.pris),
                                     //bud2 = g.forhaandsbuds.
                                     //    Where(f => f.gjenstand_id == g.gjenstand_id).
                                     //    Select(f => f.pris).ElementAtOrDefault(1)
                                 };
            List<auk_list> report_list = auksjonarius_liste.ToList();

            foreach (var item in report_list)
            {
              // IEnumerable<bud> budss = ctx.forhaandsbuds.Where(b => b.gjenstand_id == item.gjenstand_id).Select(f => f).Take(2);//.OrderByDescending(f => f.pris);
                IEnumerable<bud> buds = (from b in ctx.forhaandsbuds
                                        where b.gjenstand_id == item.gjenstand_id
                                        orderby b.pris descending
                                        select new bud
                                        {
                                            id = b.gjenstand_id,
                                            pris = b.pris.ToString(),
                                            //budgiver = b.medlem_id
                                            budgiver = ctx.medlems.Where( m => m.ID == b.medlem_id).FirstOrDefault().medlem_nummer
                                        });
              
                item.bud1 = buds.DefaultIfEmpty(new bud {id = 0, pris = "", budgiver = 0 }).First().pris.ToString();
                item.budgiver1 = buds.DefaultIfEmpty(new bud {id = 0, pris = "", budgiver = 0 }).First().budgiver.ToString();
                if (buds.Count() > 1)
                    item.bud2 = buds.ElementAt(1).pris;
                if (item.minstepris == "0")
                    item.minstepris = "";
                if (item.budgiver1 == "0")
                    item.budgiver1 = ""; 
                if (item.bud1 == "0")
                    item.bud1 = "";
                if (item.bud2 == "0")
                    item.bud2 = "";
            }

            auk_listBindingSource.DataSource = report_list;
//            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }
    }

    public class bud
    {
        public bud() { id = 0; pris = ""; }
        public int id { get; set; }
        public string pris { get; set; }
        public int budgiver { get; set; }
    }

    public class auk_list
    {
        public int gjenstand_id { get; set; }
        public int utrop_nr { get; set; }
        public string navn { get; set; }
        public string selger { get; set; }
        public string vurdering { get; set; }
        public string minstepris { get; set; }
        public string bud1 { get; set; }
        public string bud2 { get; set; }
        public string budgiver1 { get; set; }
        public string pris { get; set; }
        public string kjoper { get; set; }
    }
    //public class auk_list
    //{
    //    public int gjenstand_id { get; set; }
    //    public int utrop_nr { get; set; }
    //    public string navn { get; set; }
    //    public decimal minstepris { get; set; }
    //    public decimal bud1 { get; set; }
    //    public decimal bud2 { get; set; }
    //}
}
