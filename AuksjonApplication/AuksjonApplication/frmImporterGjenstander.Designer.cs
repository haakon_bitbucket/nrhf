﻿namespace AuksjonApplication
{
    partial class frmImporterGjenstander
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openDataFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tbPreview = new System.Windows.Forms.TextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnImporter = new System.Windows.Forms.Button();
            this.cbHeaderLines = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // openDataFileDialog
            // 
            this.openDataFileDialog.DefaultExt = "csv";
            this.openDataFileDialog.FileName = "openFileDialog1";
            this.openDataFileDialog.InitialDirectory = "%USER%";
            this.openDataFileDialog.Title = "Importer datafil";
            // 
            // tbPreview
            // 
            this.tbPreview.Location = new System.Drawing.Point(3, 67);
            this.tbPreview.Multiline = true;
            this.tbPreview.Name = "tbPreview";
            this.tbPreview.ReadOnly = true;
            this.tbPreview.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbPreview.Size = new System.Drawing.Size(912, 294);
            this.tbPreview.TabIndex = 0;
            this.tbPreview.WordWrap = false;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(12, 12);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(64, 30);
            this.btnOpenFile.TabIndex = 1;
            this.btnOpenFile.Text = "&Open File";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnImporter
            // 
            this.btnImporter.Location = new System.Drawing.Point(95, 12);
            this.btnImporter.Name = "btnImporter";
            this.btnImporter.Size = new System.Drawing.Size(75, 30);
            this.btnImporter.TabIndex = 2;
            this.btnImporter.Text = "&Importer";
            this.btnImporter.UseVisualStyleBackColor = true;
            this.btnImporter.Click += new System.EventHandler(this.btnImporter_Click);
            // 
            // cbHeaderLines
            // 
            this.cbHeaderLines.AccessibleDescription = "";
            this.cbHeaderLines.FormattingEnabled = true;
            this.cbHeaderLines.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.cbHeaderLines.Location = new System.Drawing.Point(260, 18);
            this.cbHeaderLines.Name = "cbHeaderLines";
            this.cbHeaderLines.Size = new System.Drawing.Size(34, 21);
            this.cbHeaderLines.TabIndex = 3;
            this.cbHeaderLines.Text = "1";
            this.cbHeaderLines.SelectedIndexChanged += new System.EventHandler(this.cbHeaderLines_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Header lines:";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(314, 18);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(320, 33);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Importer liste fra semikolon-sepparert fil:\r\nPrioritet; Navn; Vurdering; Minstepr" +
    "is; SelgerNr;";
            // 
            // frmImporterGjenstander
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 360);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbHeaderLines);
            this.Controls.Add(this.btnImporter);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.tbPreview);
            this.Name = "frmImporterGjenstander";
            this.Text = "Importer gjenstander";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openDataFileDialog;
        private System.Windows.Forms.TextBox tbPreview;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnImporter;
        private System.Windows.Forms.ComboBox cbHeaderLines;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
    }
}