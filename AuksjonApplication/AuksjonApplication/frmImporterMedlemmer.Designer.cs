﻿namespace AuksjonApplication
{
    partial class frmImporterMedlemmer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPreview = new System.Windows.Forms.TextBox();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnImporter = new System.Windows.Forms.Button();
            this.cbHeaderLines = new System.Windows.Forms.ComboBox();
            this.tbCbHeaderLines = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // tbPreview
            // 
            this.tbPreview.Location = new System.Drawing.Point(21, 70);
            this.tbPreview.Multiline = true;
            this.tbPreview.Name = "tbPreview";
            this.tbPreview.Size = new System.Drawing.Size(938, 382);
            this.tbPreview.TabIndex = 0;
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.Location = new System.Drawing.Point(483, 25);
            this.textBoxInfo.Multiline = true;
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.Size = new System.Drawing.Size(354, 38);
            this.textBoxInfo.TabIndex = 1;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(12, 25);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 39);
            this.btnOpenFile.TabIndex = 2;
            this.btnOpenFile.Text = "Åpne fil";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnImporter
            // 
            this.btnImporter.Location = new System.Drawing.Point(111, 25);
            this.btnImporter.Name = "btnImporter";
            this.btnImporter.Size = new System.Drawing.Size(75, 39);
            this.btnImporter.TabIndex = 3;
            this.btnImporter.Text = "Importer";
            this.btnImporter.UseVisualStyleBackColor = true;
            this.btnImporter.Click += new System.EventHandler(this.btnImporter_Click);
            // 
            // cbHeaderLines
            // 
            this.cbHeaderLines.FormattingEnabled = true;
            this.cbHeaderLines.Location = new System.Drawing.Point(313, 43);
            this.cbHeaderLines.Name = "cbHeaderLines";
            this.cbHeaderLines.Size = new System.Drawing.Size(56, 21);
            this.cbHeaderLines.TabIndex = 4;
            this.cbHeaderLines.Text = "1";
            // 
            // tbCbHeaderLines
            // 
            this.tbCbHeaderLines.Location = new System.Drawing.Point(221, 44);
            this.tbCbHeaderLines.Name = "tbCbHeaderLines";
            this.tbCbHeaderLines.Size = new System.Drawing.Size(81, 20);
            this.tbCbHeaderLines.TabIndex = 5;
            this.tbCbHeaderLines.Text = "Header lines";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // frmImporterMedlemmer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 464);
            this.Controls.Add(this.tbCbHeaderLines);
            this.Controls.Add(this.cbHeaderLines);
            this.Controls.Add(this.btnImporter);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.tbPreview);
            this.Name = "frmImporterMedlemmer";
            this.Text = "frmImporterMedlemmer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPreview;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnImporter;
        private System.Windows.Forms.ComboBox cbHeaderLines;
        private System.Windows.Forms.TextBox tbCbHeaderLines;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}