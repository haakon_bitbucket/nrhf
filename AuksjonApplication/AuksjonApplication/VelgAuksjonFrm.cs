﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonApplication
{
    public partial class VelgAuksjonFrm : Form
    {
        AuksjonDataClassesDataContext ctx;
        BindingSource bs = new BindingSource();
        public VelgAuksjonFrm()
        {
            ctx = new AuksjonDataClassesDataContext();
            InitializeComponent();

            var conf = ctx.app_config_datas.First();

            IQueryable<int> auksjoner = ctx.auksjons.Select(a => a.ID);

            bs.DataSource = ctx.auksjons;
            cbAuksjonNummer.DataSource = bs;
            cbAuksjonNummer.DisplayMember = "beskrivelse";

            var curObj = bs.List.OfType<auksjon>().ToList().Find(a => a.ID == conf.current_auksjon_id);
            var curIx = bs.IndexOf(curObj);
            bs.Position = curIx;
        }

        private void btn_lagre_Click(object sender, EventArgs e)
        {
            ctx.Log = Console.Out;
            app_config_data conf = ctx.app_config_datas.First(a => a.ID == 1);
            auksjon current = (auksjon)bs.Current;
            conf.current_auksjon_id = current.ID;

            var cs = ctx.GetChangeSet();

            ctx.SubmitChanges();

            AuksjonMain.auksjonsnavn = "Auksjon " + current.beskrivelse + " " + current.dato.ToString();

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void btn_avbryt_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
