﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonApplication
{
    public partial class ForhandsbudFrm : Form
    {
        int gjenstand_id_ = 0;
        AuksjonDataClassesDataContext db_;

        BindingSource bsBud_ = new BindingSource();
        IQueryable<forhaandsbud_t> bud_;

        public ForhandsbudFrm(int gjenstand_id)
        {
            InitializeComponent();
            gjenstand_id_ = gjenstand_id;
        }

        private void ForhandsbudFrm_Load(object sender, EventArgs e)
        {
            db_ = new AuksjonDataClassesDataContext();

            var gj = db_.gjenstands.Single(p => p.ID == gjenstand_id_);

            gjenstand_id_lbl.Text = gj.utrop_nummer.ToString();
            gjenstand_navn_lbl.Text = gj.navn;
            lbl_selger.Text = gj.selger_nummer.ToString();
            lbl_minstepris.Text = (gj.minstepris ?? 0).ToString();

            UpdateBsBud(gjenstand_id_);

            forhandsbudDGV.AllowUserToAddRows = false;
            forhandsbudDGV.AllowUserToDeleteRows = false;
            forhandsbudDGV.DataSource = bsBud_;   // forhaandsbud_;
            forhandsbudDGV.Columns["gjenstand_id"].Visible = false;
            forhandsbudDGV.Columns["kjoper_id"].Visible = false;
            //forhandsbudDGV.Columns["utrop_nr"].
            //forhandsbudDGV.Columns["kjoper_nr"].HeaderText = "Kjøper Nr";
            //forhandsbudDGV.Columns["bud"].HeaderText = "Bud";
            //forhandsbudDGV.Columns["kommentar"].HeaderText = "Kommentar";
            forhandsbudDGV.AutoResizeColumns();
            forhandsbudDGV.AllowUserToDeleteRows = false;
            forhandsbudDGV.AllowUserToAddRows = false;
            forhandsbudDGV.Dock = DockStyle.Bottom;
        }
        private void UpdateBsBud(int id)
        {
            bud_ = db_.forhaandsbuds.Where(f => f.gjenstand_id == id).
               Select(f => new forhaandsbud_t
               {
                   gjenstand_id = f.gjenstand_id,
                   kjoper_id = f.medlem_id,
                   kjoper_nr = f.medlem.medlem_nummer,
                   bud = f.pris ?? 0,
                   kommentar = f.kommentar
               });

            bsBud_.DataSource = bud_;
        }
        private void forhandsbudDGV_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
           // this.forhaandsbudTableAdapter.Update(this.nrhf_auksjonDataSet.forhaandsbud);
        }
        private void btn_NyttBud_Click(object sender, EventArgs e)
        {
            var res_nytt_bud = new nyttForhandsbud(db_.gjenstands.First(g => g.ID == gjenstand_id_));
            var res = res_nytt_bud.ShowDialog();
            if (res == DialogResult.OK)
            {
                forhaandsbud bud = res_nytt_bud.nytt_bud;
                //Insert nytt_bud
                try
                {
                    db_.Log = Console.Out;
                    db_.forhaandsbuds.InsertOnSubmit(bud);
                    db_.SubmitChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Forhåndsbud fra dette medlemet er allerede registrert for denne gjenstanden\n" + ex.Message, "Finnes fra før, avvises", MessageBoxButtons.OK);
                }
            }
            else
            {

            }
            UpdateBsBud(gjenstand_id_);
        }

        private void btn_slett_Click(object sender, EventArgs e)
        {
            forhaandsbud_t cur_bud = (forhaandsbud_t)bsBud_.Current;
            try
            {
                var slettes = db_.forhaandsbuds.First(b => b.gjenstand_id == cur_bud.gjenstand_id && b.medlem_id == cur_bud.kjoper_id);
                var mb_res = MessageBox.Show("Sikker på at du vil slette dette budet?\nGjenstand: " + slettes.gjenstand.utrop_nummer + "\nBudgiver: " + cur_bud.kjoper_nr
                    , "Slette forhåndsbud", MessageBoxButtons.YesNo);
                if (mb_res == DialogResult.Yes)
                {
                    db_.forhaandsbuds.DeleteOnSubmit(slettes);
                    db_.SubmitChanges();
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Programmet fortsetter.\nDatabasefeil:\n" + ex.Message, "Databaseoperasjone feilet.", MessageBoxButtons.OK);
            }
            UpdateBsBud(gjenstand_id_);
        }

        private void btn_endre_Click(object sender, EventArgs e)
        {
            forhaandsbud_t cur_bud = (forhaandsbud_t)bsBud_.Current;
            try
            {
                var gj = db_.gjenstands.First(g => g.ID == gjenstand_id_);
                var endret_bud = new endreForhandsbud(gj);
                var res = endret_bud.ShowDialog();
                if (res == DialogResult.OK)
                {
                    var endres = db_.forhaandsbuds.First(f => f.gjenstand_id == cur_bud.gjenstand_id && f.medlem_id == cur_bud.kjoper_id);

                    var mb_res = MessageBox.Show("Sikker på at du vil endre dette budet?\nGjenstand: " 
                                                    + endres.gjenstand.utrop_nummer
                                                    + "\nBudgiver: " + endres.medlem.medlem_nummer
                                                , "Endre forhåndsbud", MessageBoxButtons.YesNo);

                    //endres.medlem_id = endret_bud.endret_bud.medlem_id;
                    endres.pris = endret_bud.endret_bud.pris;
                    db_.SubmitChanges();
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Databaseoppdatering feilet:\n" + ex.Message, "Databaseoperasjone feilet.", MessageBoxButtons.OK);
            }
            UpdateBsBud(gjenstand_id_);
        }

        private void btn_avslutt_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
