﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

/*
Knut Stormoen er feil
gjenstand_id
3355
3356
3357
3358
3359
3360
3361
3362
3363
3364
3365
3366
3367
3368
3369
*/
  
namespace AuksjonApplication
{
    class gjenstandResult
    {
        public int ID { get; set; }
        public int UtropNr { get; set; }
        public string Navn { get; set; }
        public string Beskrivelse { get; set; }
        public string Vurdering { get; set; }
        public int Prioritet { get; set; }
        public decimal Minstepris { get; set; }
        public int Selger { get; set; }
        public string Fornavn { get; set; }
        public string Etternavn { get; set; }
    }

    class Gjenstand
    {
        public CopyPasteDataGridView gjenstanderDgw;
        BindingSource bs; 
        public AuksjonDataClassesDataContext gjenstandDb;

        bool userAddedRow = false;
        int newRowId = 0;
        int default_lot_size = 12; //normalt 20;
        int default_block_size = 6; //normalt 5
        int current_auksjon_id_;
        auksjon currentAuksjon;

        public int current_auksjon_id
        {
            get
            {
                return current_auksjon_id_;
            }
            set
            {
                current_auksjon_id_ = value;
            }
        }

        public Gjenstand(AuksjonMain main)
        {
            gjenstanderDgw = new CopyPasteDataGridView();
            
            gjenstanderDgw.Location = new System.Drawing.Point(0, 0);
            gjenstanderDgw.Name = "Gjenstander";
            //gjenstanderDgw.Size = new System.Drawing.Size(800, 600);  //dock
            gjenstanderDgw.TabIndex = 0;
            //http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridview.rowheadersvisible.aspx
            gjenstanderDgw.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            gjenstanderDgw.Dock = DockStyle.Fill;
            
            main.Controls.Add(gjenstanderDgw);

            //gjenstandDb = new GjenstandDataClassesDataContext("Data Source=HLALAPTOP\\MSSQL2008;Initial Catalog=nrhf_auksjon;User ID=sa;Password=sa");
            //gjenstandDb = new GjenstandDataClassesDataContext("AuksjonApplication.Properties.Settings.nrhf_auksjonConnectionString1");
            gjenstandDb = new AuksjonDataClassesDataContext();
            if (test_connection(gjenstandDb) == false)
            {
                Application.Exit();
                return;
            }
            bs = new BindingSource();

            current_auksjon_id_ = gjenstandDb.app_config_datas.First().current_auksjon_id;
            currentAuksjon = gjenstandDb.auksjons.Single(a => a.ID == current_auksjon_id_);
            default_lot_size = currentAuksjon.lot_size ?? 0;
            default_block_size = currentAuksjon.block_size ?? 0;


            //Eventhandlers
            this.gjenstanderDgw.CellEnter += new DataGridViewCellEventHandler(gjenstanderDgw_CellEnter);
            this.gjenstanderDgw.CellLeave += new DataGridViewCellEventHandler(gjenstanderDgw_CellLeave);
            this.gjenstanderDgw.CellValidating += new DataGridViewCellValidatingEventHandler(gjenstanderDgw_CellValidating);
            this.gjenstanderDgw.CellValidated += new DataGridViewCellEventHandler(gjenstanderDgw_CellValidated);
            this.gjenstanderDgw.CellEndEdit += new DataGridViewCellEventHandler(gjenstanderDgw_CellEndEdit);
            this.gjenstanderDgw.CellValueChanged += new DataGridViewCellEventHandler(gjenstanderDgw_CellValueChanged);
            this.gjenstanderDgw.UserAddedRow += new DataGridViewRowEventHandler(gjenstanderDgw_UserAddedRow);
            this.gjenstanderDgw.RowEnter += new DataGridViewCellEventHandler(gjenstanderDgw_RowEnter);
            this.gjenstanderDgw.RowLeave += new DataGridViewCellEventHandler(gjenstanderDgw_RowLeave);
            this.gjenstanderDgw.RowValidating += new DataGridViewCellCancelEventHandler(gjenstanderDgw_RowValidating);
            this.gjenstanderDgw.RowValidated += new DataGridViewCellEventHandler(gjenstanderDgw_RowValidated);
            this.gjenstanderDgw.UserDeletingRow += new DataGridViewRowCancelEventHandler(gjenstanderDgw_UserDeletingRow);
            this.gjenstanderDgw.UserDeletedRow += new DataGridViewRowEventHandler(gjenstanderDgw_UserDeletedRow);
       
            select();
            if (gjenstanderDgw.Rows.Count > 0)
                gjenstanderDgw.Rows[0].Frozen = true;    //???
        }

        bool test_connection(AuksjonDataClassesDataContext ctx)
        {
            try
            {
                int timeout = ctx.CommandTimeout;
                ctx.CommandTimeout = 5;
                var res = ctx.gjenstands.Take(1);
                res.ToList();
                ctx.CommandTimeout = timeout;
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Database tilkobling feilet.\n" + ex.Message);
            }
            return false;
        }


        void select()
        {
            var gjenstander = from gj in gjenstandDb.gjenstands
                              join md in gjenstandDb.medlems
                              on gj.selger_nummer equals md.medlem_nummer into mittsett
                              from md in mittsett.DefaultIfEmpty()
                              where gj.auksjon_id == current_auksjon_id_ 
                              orderby gj.selger_nummer
                              select new gjenstandResult
                              {
                                  ID = gj.ID,
                                  UtropNr = (int)gj.utrop_nummer.GetValueOrDefault(0),
                                  Navn = (string)gj.navn,
                                  Beskrivelse = (string)gj.beskrivelse,
                                  Vurdering = (string)gj.vurdering,
                                  Prioritet = (int)gj.prioritet.GetValueOrDefault(),
                                  Minstepris = (decimal)gj.minstepris.GetValueOrDefault(),
                                  Selger = (int)gj.selger_nummer.GetValueOrDefault(),
                                  Fornavn = (string)md.fornavn,
                                  Etternavn = (string)md.etternavn
                              };

            try
            {
                bs.DataSource = gjenstander;
                gjenstanderDgw.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Database feil:  " + ex.Message);
                Application.Exit();
            }
        }

        private void gjenstanderDgw_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
          
        }

        private void gjenstanderDgw_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            if (userAddedRow)
            {
                var lastRowId = (from gj_ids in gjenstandDb.gjenstands select gj_ids.ID).Max();
                newRowId = lastRowId + 1;
                dgw[0, e.RowIndex].Value = newRowId;
            }
        }

        private void gjenstanderDgw_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            dgw[e.ColumnIndex, e.RowIndex].ErrorText = string.Empty;
        }

        private void gjenstanderDgw_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            if (dgw.IsCurrentCellDirty == false)
                return;
            if (!dgw.Columns[e.ColumnIndex].HeaderText.Equals("Selger")) 
                return;

            if (String.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                gjenstanderDgw["Selger", e.RowIndex].ErrorText = "Medlemsnummer må fylles ut";
                e.Cancel = true;
                return;
            }
            int nr;
            Int32.TryParse((string)e.FormattedValue, out nr);
            if (nr == 0)
            {
                gjenstanderDgw["Selger", e.RowIndex].ErrorText = "Medlemsnummer må fylles ut";
                e.Cancel = true;
                return;
            }

            auksjonDataAksessLib.medlem etMedlem = gjenstandDb.medlems.SingleOrDefault(p => p.ID == nr);

            if (etMedlem == null)
            {
                gjenstanderDgw["Selger", e.RowIndex].ErrorText = "Ugyldig medlemsnummer " + nr;
                gjenstanderDgw.Rows[e.RowIndex].ErrorText = "Ugyldg data på rad " + gjenstanderDgw.CurrentRow;
                gjenstanderDgw.Refresh();
                e.Cancel = true;
                return;
            }
           return;
        }

        private void gjenstanderDgw_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            dgw[e.ColumnIndex, e.RowIndex].ErrorText = string.Empty;
            if (!dgw.Columns[e.ColumnIndex].HeaderText.Equals("Selger"))
                return;
            
            int nr = Int32.Parse((string)dgw[e.ColumnIndex, e.RowIndex].FormattedValue);
            auksjonDataAksessLib.medlem etMedlem = gjenstandDb.medlems.SingleOrDefault(p => p.ID == nr);

            if (etMedlem != null)
            {
                gjenstanderDgw["Fornavn", e.RowIndex].Value = etMedlem.fornavn;
                gjenstanderDgw["Etternavn", e.RowIndex].Value = etMedlem.etternavn;
                gjenstanderDgw.Refresh();
            }
            return;
        }

        private void gjenstanderDgw_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            dgw.Rows[e.RowIndex].ErrorText = string.Empty;
        }

        private void gjenstanderDgw_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridView dgw = (DataGridView)sender;
            //if ()
            //{
            //   dgw.Sort();
            //}
        }

        private void gjenstanderDgw_RowValidating(Object sender, DataGridViewCellCancelEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;

            if (String.IsNullOrEmpty(dgw["Selger", e.RowIndex].ToString()))
            {       
                e.Cancel = true;
                dgw["Selger", e.RowIndex].ErrorText = "Mangler Medlem nummer";
                return;
            }
            int nr;
            Int32.TryParse((string)dgw["Selger", e.RowIndex].FormattedValue, out nr);
            if (nr == 0)
            {
                e.Cancel = true;
                dgw["Selger", e.RowIndex].ErrorText = "Mangler Medlem nummer";
                return;
            }
        }

        private void gjenstanderDgw_RowValidated(Object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            dgw.Rows[e.RowIndex].ErrorText = string.Empty;

            //do the save
            
            if (userAddedRow)
            {
                userAddedRow = false;
                auksjonDataAksessLib.gjenstand enGjenstand = new auksjonDataAksessLib.gjenstand();

                enGjenstand.ID = newRowId;
                enGjenstand.navn = (string)gjenstanderDgw["Navn", e.RowIndex].Value;
                enGjenstand.beskrivelse = (string)gjenstanderDgw["Beskrivelse", e.RowIndex].Value;
                enGjenstand.vurdering = (string)gjenstanderDgw["Vurdering", e.RowIndex].Value;
                enGjenstand.minstepris = (decimal)gjenstanderDgw["Minstepris", e.RowIndex].Value;
                enGjenstand.prioritet = (int)gjenstanderDgw["Prioritet", e.RowIndex].Value;
                enGjenstand.selger_nummer = (int)gjenstanderDgw["Selger", e.RowIndex].Value;
                gjenstandDb.gjenstands.InsertOnSubmit(enGjenstand);
                //dgw.InvalidateCell(0, e.RowIndex);
            }
            else
            {
                int gj_id = (int)dgw["ID", e.RowIndex].Value;
                auksjonDataAksessLib.gjenstand enGjenstand = gjenstandDb.gjenstands.SingleOrDefault(p => p.ID== gj_id);
                if (enGjenstand == null)
                    return;
                enGjenstand.navn = (string)gjenstanderDgw["Navn", e.RowIndex].Value;
                enGjenstand.beskrivelse = (string)gjenstanderDgw["Beskrivelse", e.RowIndex].Value;
                enGjenstand.vurdering = (string)gjenstanderDgw["Vurdering", e.RowIndex].Value;
                enGjenstand.minstepris = (decimal)gjenstanderDgw["Minstepris", e.RowIndex].Value;
                enGjenstand.prioritet = (int)gjenstanderDgw["Prioritet", e.RowIndex].Value;
                enGjenstand.selger_nummer = (int)gjenstanderDgw["Selger", e.RowIndex].Value;
                gjenstandDb.SubmitChanges();
            }
            try
            {
                gjenstandDb.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Feil ved oppdatering av databasen:\r" + ex.Message);
            }
         }

        private void gjenstanderDgw_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            if (userAddedRow)
            {
            }
        }

        private void gjenstanderDgw_RowLeave(Object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgw = (DataGridView)sender;
            dgw.Rows[e.RowIndex].ErrorText = string.Empty;
            if (e.RowIndex == dgw.NewRowIndex)  //userAddedRow)//false == ((DataGridView)sender).IsCurrentRowDirty)
            {
                for (int ix = 0; ix < dgw.Rows[e.RowIndex].Cells.Count; ++ix)
                {
                    dgw[ix, dgw.NewRowIndex].ErrorText = string.Empty;
                }
                userAddedRow = false;
            }
        }

        private void gjenstanderDgw_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            userAddedRow = true;
        }

        private void gjenstanderDgw_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DataGridView dgw = (DataGridView)(sender);
            int gj_id = (int)dgw["ID", e.Row.Index].Value;
            if (gj_id != 0)
            {
                DialogResult confirmed = MessageBox.Show("Vil du slette raden?", "Slette data", MessageBoxButtons.YesNo);
                if (confirmed == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
                auksjonDataAksessLib.gjenstand enGjenstand = gjenstandDb.gjenstands.SingleOrDefault(p => p.ID== gj_id);
                if (enGjenstand != null)
                {
                    gjenstandDb.gjenstands.DeleteOnSubmit(enGjenstand);
                    gjenstandDb.SubmitChanges();
                }
            }
        }

        private void gjenstanderDgw_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DataGridView dgw = (DataGridView)(sender);
            gjenstandDb.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, gjenstandDb.gjenstands);
        }

        class selger_med_gjenstander
        {
            public int ID { get; set; }
            public int? MedlemNr { get; set; }
            public int? MaxLotSize { get; set; }
            public int? Prioritet { get; set; }
            public List<int> gjenstander { get; set; }
            public int BlockSize { get; set; }
            public int SplitPrioritizedLot { get; set; }
        }


        class trekning_rekkefølge
        {
            public int ID { get; set; }
            public int utrop_nr { get; set; }
        }

        IList<int> selgere_randomisert()
        {
            //Alle selgere uten prioritet
            var uprioriterte_selgere =
                            (from gj in gjenstandDb.gjenstands
                             join md in gjenstandDb.medlems
                             on gj.selger_nummer equals md.medlem_nummer into mittsett
                             from md in mittsett.DefaultIfEmpty()
                             where md.utrop_prioritet == 0 || md.utrop_prioritet == null && gj.auksjon_id == current_auksjon_id_
                             select gj.selger_nummer).Distinct();

            //Randomiser listen
            var ordered_uprioriterte = uprioriterte_selgere.ToList();     //TODO: SJEKK DATA GYLDIG
            var random_uprioriterte = Util<int?>.Shuffle(ordered_uprioriterte);
            
            //Finn alle prioriterte selgere
            var prioriterte_selgere =
                            (from gj in gjenstandDb.gjenstands
                             join md in gjenstandDb.medlems
                             on gj.selger_nummer equals md.medlem_nummer into mittsett
                             from md in mittsett.DefaultIfEmpty()
                             where md.utrop_prioritet != 0 && md.utrop_prioritet != null && gj.auksjon_id == current_auksjon_id_
                             orderby md.utrop_prioritet ascending
                             select md.medlem_nummer).Distinct();
            
            ////Start listen med prioriterte selger
            List<int> selgere_random = prioriterte_selgere.ToList();
            
            ////Legg til alle andre selgere, randomisert.
            foreach (int p in random_uprioriterte)
                selgere_random.Add(p);

            return selgere_random;
        }

        public void trekk_utropsliste()
        {
            var selgere_random = selgere_randomisert();

            //Vis rekkefølgen og bekreft
            string trekning_msg = "";
            foreach (int p in selgere_random)
            {
                trekning_msg += (p + " ");
            }
            trekning_msg += "\r\rTrekning OK?";
            List<selger_med_gjenstander> trekning_selger_gjenstander = new List<selger_med_gjenstander>();
            if (DialogResult.Yes == MessageBox.Show(trekning_msg, "Trekning selgere", MessageBoxButtons.YesNo))
            {
                foreach (int selger_nummer in selgere_random)
                {
                    var selger_info = 
                        (from s_medlemer in gjenstandDb.medlems
                        where s_medlemer.medlem_nummer == selger_nummer
                        //For BlockSize, pick default if val is null or 0 og s_medlemer.block_size = 0
                        select new selger_med_gjenstander
                        {
                            ID = s_medlemer.medlem_nummer,
                            MaxLotSize = s_medlemer.max_gjenstander.GetValueOrDefault(default_lot_size),
                            MedlemNr = s_medlemer.medlem_nummer,
                            Prioritet = s_medlemer.utrop_prioritet,
                            BlockSize = (s_medlemer.block_size == 0 || s_medlemer.block_size == null) ? default_block_size : (s_medlemer.block_size ?? default_block_size),
                            SplitPrioritizedLot = (int)((s_medlemer.split_prioritized_lot == null) ? 0 : s_medlemer.split_prioritized_lot)
                        }).Take(1);

                    if (selger_info.Count() == 0)   //Abort if selger does not exist
                    {
                        string err_msg =  "Finner ikke medlem nummer " + selger_nummer + ".\nAvbryter trekning";
                        MessageBox.Show(err_msg, "Selger finnes ikke i databasen.", MessageBoxButtons.OK);
                        return;
                    }

                    selger_med_gjenstander ny_selger = selger_info.First();

                    int lot_size = ny_selger.MaxLotSize.GetValueOrDefault(default_lot_size); //Tar 0 som 0

                    IEnumerable<int> objekter = (from s_objekter in gjenstandDb.gjenstands
                                                 where s_objekter.selger_nummer == selger_nummer && s_objekter.auksjon_id == current_auksjon_id_
                                                 orderby s_objekter.prioritet
                                                 select s_objekter.ID).Take(lot_size);

                    ny_selger.gjenstander = objekter.ToList();

                    trekning_selger_gjenstander.Add(ny_selger);
                }

                List<int> endelig_trekning = new List<int>();   //Liste med gjenstand-id i trukket rekkefølge

                while (true)
                {
                    //Trekningsliste.
                    //Hver deltager tildeles auksjon.blocksize(default) 
                    //  eller selger.blocksize antall gjenstander
                    //Prioriterte selgere får alle gjenstander i en blokk
                    //  Eller hvis selger.SplitPrioritizedLot tildeles selger.blocksize
                    //Repeter inntil alla selger.gjenstander.Count() == 0

                    int cnt = 0;
                    foreach (selger_med_gjenstander selger in trekning_selger_gjenstander)
                    {

                        int block_size = selger.BlockSize;                                          //Spesial blocksize
                        if (selger.gjenstander.Count < block_size)
                            block_size = selger.gjenstander.Count;
                        if (selger.Prioritet > 0 && selger.SplitPrioritizedLot == 0)                //Prioriterte selger får alle gjenstander først.
                            block_size = selger.gjenstander.Count;                                  //Hvis SplitPrioritizedLot>1, del opp i flere blokker
                        for (int ix = 0; ix < block_size; ++ix)
                        {
                            endelig_trekning.Add(selger.gjenstander[ix]);
                            ++cnt;
                        }
                        selger.gjenstander.RemoveRange(0, block_size);
                    }
                    if (cnt == 0)
                        break;
                }
                
                //Show result and let user decide to abort
                string msg = "";
                foreach (int id in endelig_trekning)
                {
                    msg += id;
                    msg += " ";
                }
                msg += "\r\rLagre trekningen?";
                if (DialogResult.Yes == MessageBox.Show(msg, "Trekning", MessageBoxButtons.YesNo))
                {   
                    //Null ut alle utropsnumre => egen funksjon?
                    //IEnumerable<int> _ider = gjenstandDb.gjenstands.Select(p => p.gjenstand_id);
                    IEnumerable<int> ider = (from ids in gjenstandDb.gjenstands
                                                 where ids.auksjon_id == current_auksjon_id_
                                                 select ids.ID);
                    foreach (int enid in ider)
                    {
                        var gjenstand = gjenstandDb.gjenstands.Single(p => p.ID== enid);
                        gjenstand.utrop_nummer = 0;
                        gjenstandDb.SubmitChanges();
                        //gjenstand.utrop_nummer = 0;
                        //gjenstandDb.SubmitChanges();
                    }

                    for (int ix = 0; ix < endelig_trekning.Count; ++ix)     //Update utrop-nr in database
                    {

                        //IEnumerable<gjenstand> objekter = (from s_objekter in gjenstandDb.gjenstands
                        //                             where s_objekter.selger_id == selger_id
                        //                             select s_objekter.gjenstand_id);

                        //var gjenstand = gjenstandDb.gjenstands.Select();
                        var gjenstand = gjenstandDb.gjenstands.Single(p => p.ID == endelig_trekning[ix]);
                        gjenstand.utrop_nummer = ix + 1; //List is 0-based
                        try
                        {
                            gjenstandDb.SubmitChanges();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Databaseoppdatering feilet\r" + ex.Message);
                        }
                    }
                    select();
                }
            }
        }
    }

    public class Util<T>
    {
        private static Random rng = new Random();
        public static ICollection<T> Shuffle(ICollection<T> c)
        {
            T[] a = new T[c.Count];
            c.CopyTo(a, 0);
            byte[] b = new byte[a.Length];
            rng.NextBytes(b);
            Array.Sort(b, a);
            return new List<T>(a);
        }
    }
}
