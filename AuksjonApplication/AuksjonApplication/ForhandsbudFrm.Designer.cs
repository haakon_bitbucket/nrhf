﻿namespace AuksjonApplication
{
    partial class ForhandsbudFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gjenstand_id_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.forhandsbudDGV = new System.Windows.Forms.DataGridView();
            this.gjenstand_navn_lbl = new System.Windows.Forms.Label();
            this.btn_avslutt = new System.Windows.Forms.Button();
            this.btn_NyttBud = new System.Windows.Forms.Button();
            this.btn_slett = new System.Windows.Forms.Button();
            this.btn_endre = new System.Windows.Forms.Button();
            this.lbl_hdr_selger = new System.Windows.Forms.Label();
            this.lbl_hdr_minstepris = new System.Windows.Forms.Label();
            this.lbl_selger = new System.Windows.Forms.Label();
            this.lbl_minstepris = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.forhandsbudDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gjenstand_id_lbl
            // 
            this.gjenstand_id_lbl.AutoSize = true;
            this.gjenstand_id_lbl.Location = new System.Drawing.Point(91, 9);
            this.gjenstand_id_lbl.Name = "gjenstand_id_lbl";
            this.gjenstand_id_lbl.Size = new System.Drawing.Size(64, 13);
            this.gjenstand_id_lbl.TabIndex = 1;
            this.gjenstand_id_lbl.Text = "gjenstand id";
            this.gjenstand_id_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Gjenstand";
            // 
            // forhandsbudDGV
            // 
            this.forhandsbudDGV.AllowUserToAddRows = false;
            this.forhandsbudDGV.AllowUserToDeleteRows = false;
            this.forhandsbudDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.forhandsbudDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.forhandsbudDGV.Location = new System.Drawing.Point(0, 0);
            this.forhandsbudDGV.Name = "forhandsbudDGV";
            this.forhandsbudDGV.Size = new System.Drawing.Size(469, 230);
            this.forhandsbudDGV.TabIndex = 11;
            this.forhandsbudDGV.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.forhandsbudDGV_RowLeave);
            // 
            // gjenstand_navn_lbl
            // 
            this.gjenstand_navn_lbl.AutoSize = true;
            this.gjenstand_navn_lbl.Location = new System.Drawing.Point(172, 9);
            this.gjenstand_navn_lbl.Name = "gjenstand_navn_lbl";
            this.gjenstand_navn_lbl.Size = new System.Drawing.Size(80, 13);
            this.gjenstand_navn_lbl.TabIndex = 14;
            this.gjenstand_navn_lbl.Text = "gjenstand navn";
            this.gjenstand_navn_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_avslutt
            // 
            this.btn_avslutt.Location = new System.Drawing.Point(326, 106);
            this.btn_avslutt.Name = "btn_avslutt";
            this.btn_avslutt.Size = new System.Drawing.Size(76, 26);
            this.btn_avslutt.TabIndex = 24;
            this.btn_avslutt.Text = "&Avslutt";
            this.btn_avslutt.UseVisualStyleBackColor = true;
            this.btn_avslutt.Click += new System.EventHandler(this.btn_avslutt_Click);
            // 
            // btn_NyttBud
            // 
            this.btn_NyttBud.Location = new System.Drawing.Point(30, 104);
            this.btn_NyttBud.Name = "btn_NyttBud";
            this.btn_NyttBud.Size = new System.Drawing.Size(88, 28);
            this.btn_NyttBud.TabIndex = 29;
            this.btn_NyttBud.Text = "&Ny";
            this.btn_NyttBud.UseVisualStyleBackColor = true;
            this.btn_NyttBud.Click += new System.EventHandler(this.btn_NyttBud_Click);
            // 
            // btn_slett
            // 
            this.btn_slett.Location = new System.Drawing.Point(230, 106);
            this.btn_slett.Name = "btn_slett";
            this.btn_slett.Size = new System.Drawing.Size(75, 26);
            this.btn_slett.TabIndex = 30;
            this.btn_slett.Text = "&Slett";
            this.btn_slett.UseVisualStyleBackColor = true;
            this.btn_slett.Click += new System.EventHandler(this.btn_slett_Click);
            // 
            // btn_endre
            // 
            this.btn_endre.Location = new System.Drawing.Point(138, 106);
            this.btn_endre.Name = "btn_endre";
            this.btn_endre.Size = new System.Drawing.Size(75, 26);
            this.btn_endre.TabIndex = 31;
            this.btn_endre.Text = "&Endre";
            this.btn_endre.UseVisualStyleBackColor = true;
            this.btn_endre.Click += new System.EventHandler(this.btn_endre_Click);
            // 
            // lbl_hdr_selger
            // 
            this.lbl_hdr_selger.AutoSize = true;
            this.lbl_hdr_selger.Location = new System.Drawing.Point(24, 33);
            this.lbl_hdr_selger.Name = "lbl_hdr_selger";
            this.lbl_hdr_selger.Size = new System.Drawing.Size(37, 13);
            this.lbl_hdr_selger.TabIndex = 32;
            this.lbl_hdr_selger.Text = "Selger";
            // 
            // lbl_hdr_minstepris
            // 
            this.lbl_hdr_minstepris.AutoSize = true;
            this.lbl_hdr_minstepris.Location = new System.Drawing.Point(24, 59);
            this.lbl_hdr_minstepris.Name = "lbl_hdr_minstepris";
            this.lbl_hdr_minstepris.Size = new System.Drawing.Size(54, 13);
            this.lbl_hdr_minstepris.TabIndex = 33;
            this.lbl_hdr_minstepris.Text = "Minstepris";
            // 
            // lbl_selger
            // 
            this.lbl_selger.AutoSize = true;
            this.lbl_selger.Location = new System.Drawing.Point(91, 33);
            this.lbl_selger.Name = "lbl_selger";
            this.lbl_selger.Size = new System.Drawing.Size(37, 13);
            this.lbl_selger.TabIndex = 34;
            this.lbl_selger.Text = "Selger";
            // 
            // lbl_minstepris
            // 
            this.lbl_minstepris.AutoSize = true;
            this.lbl_minstepris.Location = new System.Drawing.Point(91, 59);
            this.lbl_minstepris.Name = "lbl_minstepris";
            this.lbl_minstepris.Size = new System.Drawing.Size(31, 13);
            this.lbl_minstepris.TabIndex = 35;
            this.lbl_minstepris.Text = "____";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btn_avslutt);
            this.splitContainer1.Panel1.Controls.Add(this.lbl_minstepris);
            this.splitContainer1.Panel1.Controls.Add(this.btn_slett);
            this.splitContainer1.Panel1.Controls.Add(this.btn_endre);
            this.splitContainer1.Panel1.Controls.Add(this.lbl_hdr_minstepris);
            this.splitContainer1.Panel1.Controls.Add(this.lbl_selger);
            this.splitContainer1.Panel1.Controls.Add(this.lbl_hdr_selger);
            this.splitContainer1.Panel1.Controls.Add(this.btn_NyttBud);
            this.splitContainer1.Panel1.Controls.Add(this.gjenstand_navn_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.gjenstand_id_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.forhandsbudDGV);
            this.splitContainer1.Size = new System.Drawing.Size(469, 393);
            this.splitContainer1.SplitterDistance = 159;
            this.splitContainer1.TabIndex = 36;
            // 
            // ForhandsbudFrm
            // 
            this.AcceptButton = this.btn_avslutt;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 393);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ForhandsbudFrm";
            this.Text = "Forhandsbud";
            this.Load += new System.EventHandler(this.ForhandsbudFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.forhandsbudDGV)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label gjenstand_id_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView forhandsbudDGV;
        private System.Windows.Forms.Label gjenstand_navn_lbl;
        private System.Windows.Forms.Button btn_avslutt;
        private System.Windows.Forms.Button btn_NyttBud;
        private System.Windows.Forms.Button btn_slett;
        private System.Windows.Forms.Button btn_endre;
        private System.Windows.Forms.Label lbl_hdr_selger;
        private System.Windows.Forms.Label lbl_hdr_minstepris;
        private System.Windows.Forms.Label lbl_selger;
        private System.Windows.Forms.Label lbl_minstepris;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}