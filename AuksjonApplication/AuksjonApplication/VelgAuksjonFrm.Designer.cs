﻿namespace AuksjonApplication
{
    partial class VelgAuksjonFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbAuksjonNummer = new System.Windows.Forms.ComboBox();
            this.btn_lagre = new System.Windows.Forms.Button();
            this.btn_avbryt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbAuksjonNummer
            // 
            this.cbAuksjonNummer.FormattingEnabled = true;
            this.cbAuksjonNummer.Location = new System.Drawing.Point(27, 55);
            this.cbAuksjonNummer.Name = "cbAuksjonNummer";
            this.cbAuksjonNummer.Size = new System.Drawing.Size(121, 21);
            this.cbAuksjonNummer.TabIndex = 0;
            // 
            // btn_lagre
            // 
            this.btn_lagre.Location = new System.Drawing.Point(187, 55);
            this.btn_lagre.Name = "btn_lagre";
            this.btn_lagre.Size = new System.Drawing.Size(61, 21);
            this.btn_lagre.TabIndex = 1;
            this.btn_lagre.Text = "Lagre";
            this.btn_lagre.UseVisualStyleBackColor = true;
            this.btn_lagre.Click += new System.EventHandler(this.btn_lagre_Click);
            // 
            // btn_avbryt
            // 
            this.btn_avbryt.Location = new System.Drawing.Point(187, 82);
            this.btn_avbryt.Name = "btn_avbryt";
            this.btn_avbryt.Size = new System.Drawing.Size(61, 21);
            this.btn_avbryt.TabIndex = 2;
            this.btn_avbryt.Text = "Avbryt";
            this.btn_avbryt.UseVisualStyleBackColor = true;
            this.btn_avbryt.Click += new System.EventHandler(this.btn_avbryt_Click);
            // 
            // VelgAuksjonFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 173);
            this.Controls.Add(this.btn_avbryt);
            this.Controls.Add(this.btn_lagre);
            this.Controls.Add(this.cbAuksjonNummer);
            this.Name = "VelgAuksjonFrm";
            this.Text = "VelgAuksjonFrm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbAuksjonNummer;
        private System.Windows.Forms.Button btn_lagre;
        private System.Windows.Forms.Button btn_avbryt;
    }
}