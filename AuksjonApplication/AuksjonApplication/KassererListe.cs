﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonApplication
{
    public partial class KassererListe : Form
    {
        public AuksjonDataClassesDataContext ctx = new AuksjonDataClassesDataContext();
        public IEnumerable<auk_list> kasserer_liste;
        public int current_auksjon_id;

        public KassererListe()
        {
            current_auksjon_id = ctx.app_config_datas.Single().current_auksjon_id;
            InitializeComponent();
        }

        private void KassererListe_Load(object sender, EventArgs e)
        {
            kasserer_liste = from g in ctx.gjenstands
                                 where g.auksjon_id == current_auksjon_id
                                 orderby (g.utrop_nummer)
                                 select new auk_list
                                 {
                                     gjenstand_id = g.ID,
                                     utrop_nr = g.utrop_nummer ?? 0,
                                     navn = g.navn,
                                     vurdering = g.vurdering,
                                     selger = g.selger_nummer.ToString(),
                                     minstepris = g.minstepris.ToString(),
                                     pris = g.pris.ToString(),
                                     kjoper = g.kjoper_nummer.ToString()
                                 };
            List<auk_list> report_list = kasserer_liste.ToList();

            foreach (var item in report_list)
            {
                // IEnumerable<bud> budss = ctx.forhaandsbuds.Where(b => b.gjenstand_id == item.gjenstand_id).Select(f => f).Take(2);//.OrderByDescending(f => f.pris);

                IEnumerable<bud> buds = from b in ctx.forhaandsbuds
                                         where (b.gjenstand_id == item.gjenstand_id)
                                         orderby b.pris descending
                                         select new bud
                                         {
                                             id = b.gjenstand_id,
                                             pris = b.pris.ToString(),
                                             //budgiver = b.medlem_id
                                             budgiver = ctx.medlems.Where(m => m.ID == b.medlem_id).FirstOrDefault().medlem_nummer
                                         };

                item.bud1 = buds.DefaultIfEmpty(new bud { id = 0, pris = "", budgiver = 0 }).First().pris.ToString();
                item.budgiver1 = buds.DefaultIfEmpty(new bud { id = 0, pris = "", budgiver = 0 }).First().budgiver.ToString();
                if (buds.Count() > 1)
                    item.bud2 = buds.ElementAt(1).pris;
                if (item.minstepris == "0")
                    item.minstepris = "";
                if (item.budgiver1 == "0")
                    item.budgiver1 = "";
                if (item.bud1 == "0")
                    item.bud1 = "";
                if (item.bud2 == "0")
                    item.bud2 = "";
                if (item.pris == "0")
                    item.pris = "";
                if (item.kjoper == "0")
                    item.kjoper = "";
            }
 
            auk_listBindingSource.DataSource = report_list;
            this.reportViewer1.RefreshReport();
        }
    }
}
