﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using auksjonDataAksessLib;

namespace AuksjonApplication
{
    public partial class frmImporterGjenstander : Form
    {
        AuksjonDataClassesDataContext ctx;
        List<string[]> lines;
        int currentAuksjon;

        public frmImporterGjenstander()
        {
            ctx = new AuksjonDataClassesDataContext();
            InitializeComponent();
            currentAuksjon = ctx.app_config_datas.First().current_auksjon_id;
        }

        private void cbHeaderLines_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        //Åpne en csv fil med gejnstander. Bruk encoding for å få norske tegn korrekt.
        //Skriv ut innhold i preview
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != openDataFileDialog.ShowDialog())
                return;
            try
            {
                StreamReader fs = new StreamReader(openDataFileDialog.OpenFile(), System.Text.Encoding.GetEncoding(1252));
                lines = new List<string[]>();

                // Read and display lines from the file until the end of file
                Cursor saveCur = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                while (!fs.EndOfStream)
                {
                    string line = fs.ReadLine();
                    lines.Add(line.Split(';'));//.Select().ToArray());
                    tbPreview.Text += (line + Environment.NewLine);
                }
                Cursor.Current = saveCur;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Kan ikke åpne filen");
            }

        }

        //Insert alle elementer i 'lines'. Skip header n lines.
        private void btnImporter_Click(object sender, EventArgs e)
        {
            auksjonDataAksessLib.auksjon auk = ctx.auksjons.Single(a => a.ID == currentAuksjon);
            if (auk.utropsliste_laast == 1)
            {
                if (DialogResult.Yes == MessageBox.Show("Listen er låst\r\nVil du låse opp listen og importere på nytt?", "Låst gjenstandsliste", MessageBoxButtons.YesNo))
                {
                    if (DialogResult.Yes == MessageBox.Show("Listen blir overskrevet. Sikker??", "Låst gjenstandsliste", MessageBoxButtons.YesNo))
                    {
                    }
                    else return;
                }
                else return;
            }

            int skipHeaderLines = int.Parse(cbHeaderLines.Text);

            Cursor saveCur = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            int current_line = 0;

            try
            {
                auk.utropsliste_laast = 0;
                ctx.SubmitChanges();
//Prioritet;Navn;Vurdering;Minstepris;SelgerNr;
                foreach (string[] strarr in lines)
                {
                    if (skipHeaderLines > 0)
                    {
                        --skipHeaderLines;
                        continue;
                    }
                    ++current_line;
                    auksjonDataAksessLib.gjenstand nyGj = new auksjonDataAksessLib.gjenstand();
                    nyGj.auksjon_id = currentAuksjon; ;
                    nyGj.prioritet = int.Parse(strarr[0]);
                    nyGj.navn = strarr[1];
                    nyGj.vurdering = strarr[2];
                    int minstepris;
                    if (int.TryParse(strarr[3], out minstepris))
                        nyGj.minstepris = minstepris;
                    nyGj.selger_nummer = int.Parse(strarr[4]);
                    ctx.gjenstands.InsertOnSubmit(nyGj);
                }
                auk.utropsliste_laast = 1;
                ctx.SubmitChanges();
                Cursor.Current = saveCur;
                MessageBox.Show("Utropslisten er nå låst", "Import komplett", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                StringBuilder str = new StringBuilder("Import failed on line");
                str.Append(current_line);
                str.Append("\n");
                if (current_line > 0)
                {
                    str.Append(lines[current_line - 2]);
                    str.Append("\n\n");
                }
                str.Append(ex.Message);
                MessageBox.Show(str.ToString(), "Import failed.");
            }
        }
    }
}
