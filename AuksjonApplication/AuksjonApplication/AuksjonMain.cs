﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AuksjonApplication
{

    public partial class AuksjonMain : Form
    {
        public static string auksjonsnavn;
        Gjenstand gj;

        public AuksjonMain()
        {
            gj = new Gjenstand(this);
            InitializeComponent();
        }

        private void trekk_listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gj.trekk_utropsliste();
        }

        private void medlemerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void forhåndsbudToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int row = gj.gjenstanderDgw.CurrentCell.RowIndex;
            int gjenstand_id = (int)gj.gjenstanderDgw[0, row].Value;
            if (gjenstand_id == 0)
            {
                MessageBox.Show("Ingen gjenstander er lagt inn for denne auksjonen.");
            }
            else
            {
                ForhandsbudFrm fhb = new ForhandsbudFrm(gjenstand_id);
                fhb.Show();
            }
        }

        private void velgAktivAuksjonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VelgAuksjonFrm dlg = new VelgAuksjonFrm();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
               
            }
        }

        private void administrerAuksjonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdministrerAuksjonFrm adm_auksjoner = new AdministrerAuksjonFrm();
            adm_auksjoner.Show();
        }

        private void importerGjenstandslisteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImporterGjenstander imp = new frmImporterGjenstander();
            imp.Show();
        }

        private void liste1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AuksjonariusListe auklst = new AuksjonariusListe();
            auklst.Show();
        }

        private void kassererListeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KassererListe kaslst = new KassererListe();
            kaslst.Show();
        }

    }
}
