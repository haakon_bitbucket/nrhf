use nrhf_auksjon;

SELECT 
dbo.gjenstand.utrop_nummer 'Gjenstand', 
MAX(CONVERT(VARCHAR(8000),dbo.gjenstand.navn)) 'Navn',
MAX(COALESCE(dbo.gjenstand.minstepris, 0)) 'Minstepris', 
MAX(dbo.gjenstand.pris) 'Pris', 
MAX(dbo.gjenstand.selger_nummer) 'Selger', 
MAX(dbo.gjenstand.kjoper_nummer) 'Kj�per', 
MAX(coalesce(dbo.varelinje.provisjon_salg,0)) 'Provisjon selger', 
MAX(coalesce(dbo.varelinje.provisjon_kjop,0)) 'Provisjon kj�per'
FROM dbo.gjenstand
		INNER JOIN dbo.varelinje ON dbo.gjenstand.gjenstand_id = dbo.varelinje.gjenstand_id
WHERE dbo.gjenstand.selger_nummer=1 and dbo.gjenstand.auksjon_id in (
	select current_auksjon_id
	from dbo.app_config_data 
)
GROUP BY 
dbo.gjenstand.utrop_nummer
