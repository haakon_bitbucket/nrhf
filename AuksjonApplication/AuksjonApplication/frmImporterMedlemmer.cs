﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using auksjonDataAksessLib;

namespace AuksjonApplication
{
    public partial class frmImporterMedlemmer : Form
    {
        AuksjonDataClassesDataContext ctx;
        List<string[]> lines;
        int currentAuksjon;
        char separator = ';';

        public frmImporterMedlemmer()
        {
            InitializeComponent();
            ctx = new AuksjonDataClassesDataContext();
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != openFileDialog.ShowDialog())
                return;
            try
            {
                StreamReader fs = new StreamReader(openFileDialog.OpenFile(), System.Text.Encoding.GetEncoding(1252));
                lines = new List<string[]>();

                // Read and display lines from the file until the end of file
                Cursor saveCur = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                while (!fs.EndOfStream)
                {
                    string line = fs.ReadLine();
                    lines.Add(line.Split(separator));//.Select().ToArray());
                    tbPreview.Text += (line + Environment.NewLine);
                }
                Cursor.Current = saveCur;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Kan ikke åpne filen");
            }
        }

        private void btnImporter_Click(object sender, EventArgs e)
        {
            int skipHeaderLines = int.Parse(cbHeaderLines.Text);

            Cursor saveCur = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            int current_line = 0;

            try
            {
                foreach (string[] strarr in lines)
                {
                    if (skipHeaderLines > 0)
                    {
                        --skipHeaderLines;
                        continue;
                    }

                    ++current_line;
                    auksjonDataAksessLib.medlem ny_medlem = new auksjonDataAksessLib.medlem();
                    ny_medlem.medlem_nummer = int.Parse(strarr[0]);
                    ny_medlem.fornavn = strarr[1];
                    ny_medlem.mellomnavn = strarr[2];
                    ny_medlem.etternavn = strarr[3];
                    ny_medlem.gate1 = strarr[4];
                    ny_medlem.gate2 = strarr[5];
                    ny_medlem.postnummer = strarr[5];
                    ny_medlem.poststed = strarr[6];
                    ny_medlem.land = strarr[7];
                    ny_medlem.epost = strarr[8];
                    ny_medlem.kontonummer = int.Parse(strarr[10]);
                    ny_medlem.utrop_prioritet = int.Parse(strarr[11]);
                    ctx.medlems.InsertOnSubmit(ny_medlem);
                }
//                ctx.SubmitChanges();
                Cursor.Current = saveCur;
            }
            catch (Exception ex)
            {
                StringBuilder str = new StringBuilder("Import failed on line");
                str.Append(current_line);
                str.Append("\n");
                if (current_line > 0)
                {
                    str.Append(lines[current_line - 2]);
                    str.Append("\n\n");
                }
                str.Append(ex.Message);
                MessageBox.Show(str.ToString(), "Import failed.");
            }
        }
    }
}
