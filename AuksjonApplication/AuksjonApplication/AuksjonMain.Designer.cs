﻿namespace AuksjonApplication
{
    partial class AuksjonMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gjenstanderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medlemerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerGjenstandslisteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forhåndsbudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auksjonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.velgAktivAuksjonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrerAuksjonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liste1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kassererListeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cRListeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.forhåndsbudToolStripMenuItem,
            this.auksjonToolStripMenuItem,
            this.liste1ToolStripMenuItem,
            this.kassererListeToolStripMenuItem,
            this.cRListeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1075, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gjenstanderToolStripMenuItem,
            this.medlemerToolStripMenuItem,
            this.importerGjenstandslisteToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // gjenstanderToolStripMenuItem
            // 
            this.gjenstanderToolStripMenuItem.Name = "gjenstanderToolStripMenuItem";
            this.gjenstanderToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.gjenstanderToolStripMenuItem.Text = "&Trekk liste";
            this.gjenstanderToolStripMenuItem.Click += new System.EventHandler(this.trekk_listeToolStripMenuItem_Click);
            // 
            // medlemerToolStripMenuItem
            // 
            this.medlemerToolStripMenuItem.Name = "medlemerToolStripMenuItem";
            this.medlemerToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.medlemerToolStripMenuItem.Text = "&Medlemer";
            this.medlemerToolStripMenuItem.Click += new System.EventHandler(this.medlemerToolStripMenuItem_Click);
            // 
            // importerGjenstandslisteToolStripMenuItem
            // 
            this.importerGjenstandslisteToolStripMenuItem.Name = "importerGjenstandslisteToolStripMenuItem";
            this.importerGjenstandslisteToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.importerGjenstandslisteToolStripMenuItem.Text = "&Importer gjenstandsliste";
            this.importerGjenstandslisteToolStripMenuItem.Click += new System.EventHandler(this.importerGjenstandslisteToolStripMenuItem_Click);
            // 
            // forhåndsbudToolStripMenuItem
            // 
            this.forhåndsbudToolStripMenuItem.Name = "forhåndsbudToolStripMenuItem";
            this.forhåndsbudToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.forhåndsbudToolStripMenuItem.Text = "F&orhåndsbud";
            this.forhåndsbudToolStripMenuItem.Click += new System.EventHandler(this.forhåndsbudToolStripMenuItem_Click);
            // 
            // auksjonToolStripMenuItem
            // 
            this.auksjonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.velgAktivAuksjonToolStripMenuItem,
            this.administrerAuksjonToolStripMenuItem});
            this.auksjonToolStripMenuItem.Name = "auksjonToolStripMenuItem";
            this.auksjonToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.auksjonToolStripMenuItem.Text = "Auksjon";
            // 
            // velgAktivAuksjonToolStripMenuItem
            // 
            this.velgAktivAuksjonToolStripMenuItem.Name = "velgAktivAuksjonToolStripMenuItem";
            this.velgAktivAuksjonToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.velgAktivAuksjonToolStripMenuItem.Text = "Velg aktiv auksjon";
            this.velgAktivAuksjonToolStripMenuItem.Click += new System.EventHandler(this.velgAktivAuksjonToolStripMenuItem_Click);
            // 
            // administrerAuksjonToolStripMenuItem
            // 
            this.administrerAuksjonToolStripMenuItem.Name = "administrerAuksjonToolStripMenuItem";
            this.administrerAuksjonToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.administrerAuksjonToolStripMenuItem.Text = "Administrer auksjon";
            this.administrerAuksjonToolStripMenuItem.Click += new System.EventHandler(this.administrerAuksjonToolStripMenuItem_Click);
            // 
            // liste1ToolStripMenuItem
            // 
            this.liste1ToolStripMenuItem.Name = "liste1ToolStripMenuItem";
            this.liste1ToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.liste1ToolStripMenuItem.Text = "Auksjonarius liste";
            this.liste1ToolStripMenuItem.Click += new System.EventHandler(this.liste1ToolStripMenuItem_Click);
            // 
            // kassererListeToolStripMenuItem
            // 
            this.kassererListeToolStripMenuItem.Name = "kassererListeToolStripMenuItem";
            this.kassererListeToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.kassererListeToolStripMenuItem.Text = "Kasserer liste";
            this.kassererListeToolStripMenuItem.Click += new System.EventHandler(this.kassererListeToolStripMenuItem_Click);
            // 
            // cRListeToolStripMenuItem
            // 
            this.cRListeToolStripMenuItem.Name = "cRListeToolStripMenuItem";
            this.cRListeToolStripMenuItem.Size = new System.Drawing.Size(12, 20);
            // 
            // AuksjonMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1075, 614);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AuksjonMain";
            this.Text = "NRHF Auksjon - Salgsobjekter";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gjenstanderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medlemerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forhåndsbudToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auksjonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem velgAktivAuksjonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrerAuksjonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importerGjenstandslisteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liste1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kassererListeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cRListeToolStripMenuItem;
    }
}

