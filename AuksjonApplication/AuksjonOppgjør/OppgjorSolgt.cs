﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonOppgjør
{
    public partial class OppgjorSolgt : Form
    {
        AuksjonDataClassesDataContext ctx;
        BindingSource deltagere;
        BindingSource kjopere;
        BindingSource selgere;

        
        public OppgjorSolgt()
        {
            ctx = new AuksjonDataClassesDataContext();
            InitializeComponent();
        }

        private void OppgjorSolgt_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nrhf_auksjonDataSet.gjenstand' table. You can move, or remove it, as needed.
            this.gjenstandTableAdapter.Fill(this.nrhf_auksjonDataSet.gjenstand);

            this.reportViewer1.RefreshReport();
        }
    }
}
