﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AuksjonOppgjør
{
    public partial class Oppgjor : Form
    {
        public Oppgjor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OppgjorSolgt oppgj = new OppgjorSolgt();
            oppgj.Show();
        }
    }
}
