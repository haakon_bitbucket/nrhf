﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuksjonKjoperFaktura
{
    class Deltager
    {
        public Deltager(int nr, string fornavn, string mellomnavn, string etternavn, string gate1, string gate2, string postnummer, string poststed, string land)
        {
            Medlemnummer = nr;
            Fornavn = fornavn;
            Mellomnavn = mellomnavn;
            Etternavn = etternavn;
            Gate1 = gate1;
            Gate2 = gate2;
            Postnummer = postnummer;
            Poststed = poststed;
            Land = land;
        }

        public int Medlemnummer { get; set; }
        public string Fornavn { get; set; }
        public string Mellomnavn { get; set; }
        public string Etternavn { get; set; }
        public string Gate1 { get; set; }
        public string Gate2 { get; set; }
        public string Postnummer { get; set; }
        public string Poststed { get; set; }
        public string Land { get; set; }
    }
}
