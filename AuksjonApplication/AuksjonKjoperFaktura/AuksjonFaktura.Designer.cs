﻿namespace AuksjonKjoperFaktura
{
    partial class auksjonFaktura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.medlemidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kontonummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxgjenstanderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.utropprioritetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gjenstandDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fakturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_forste_medlem = new System.Windows.Forms.Button();
            this.btn_forrige_medlem = new System.Windows.Forms.Button();
            this.tb_current_medlem = new System.Windows.Forms.TextBox();
            this.btn_neste_medlem = new System.Windows.Forms.Button();
            this.btn_siste_medlem = new System.Windows.Forms.Button();
            this.oppgjorTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sumTB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nettoKjopTB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.provisjonKjopTB = new System.Windows.Forms.TextBox();
            this.bruttoKjopTB = new System.Windows.Forms.TextBox();
            this.nettoSalgTB = new System.Windows.Forms.TextBox();
            this.bruttoSalgTB = new System.Windows.Forms.TextBox();
            this.provisjonSalgTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_beregn_oppgjor = new System.Windows.Forms.Button();
            this.btn_produser_faktura = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.solgtVarelinjerDGV = new System.Windows.Forms.DataGridView();
            this.kjoperDGV = new System.Windows.Forms.DataGridView();
            this.kjoptVarelinjerDGV = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.medlemnummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fornavnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mellomnavnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etternavnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gate1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gate2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postnummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.poststedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.landDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.medlemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.varelinjeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.oppgjorTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.solgtVarelinjerDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kjoperDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kjoptVarelinjerDGV)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medlemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.varelinjeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // medlemidDataGridViewTextBoxColumn
            // 
            this.medlemidDataGridViewTextBoxColumn.DataPropertyName = "medlem_id";
            this.medlemidDataGridViewTextBoxColumn.HeaderText = "medlem_id";
            this.medlemidDataGridViewTextBoxColumn.Name = "medlemidDataGridViewTextBoxColumn";
            // 
            // kontonummerDataGridViewTextBoxColumn
            // 
            this.kontonummerDataGridViewTextBoxColumn.DataPropertyName = "kontonummer";
            this.kontonummerDataGridViewTextBoxColumn.HeaderText = "kontonummer";
            this.kontonummerDataGridViewTextBoxColumn.Name = "kontonummerDataGridViewTextBoxColumn";
            // 
            // epostDataGridViewTextBoxColumn
            // 
            this.epostDataGridViewTextBoxColumn.DataPropertyName = "epost";
            this.epostDataGridViewTextBoxColumn.HeaderText = "epost";
            this.epostDataGridViewTextBoxColumn.Name = "epostDataGridViewTextBoxColumn";
            // 
            // maxgjenstanderDataGridViewTextBoxColumn
            // 
            this.maxgjenstanderDataGridViewTextBoxColumn.DataPropertyName = "max_gjenstander";
            this.maxgjenstanderDataGridViewTextBoxColumn.HeaderText = "max_gjenstander";
            this.maxgjenstanderDataGridViewTextBoxColumn.Name = "maxgjenstanderDataGridViewTextBoxColumn";
            // 
            // utropprioritetDataGridViewTextBoxColumn
            // 
            this.utropprioritetDataGridViewTextBoxColumn.DataPropertyName = "utrop_prioritet";
            this.utropprioritetDataGridViewTextBoxColumn.HeaderText = "utrop_prioritet";
            this.utropprioritetDataGridViewTextBoxColumn.Name = "utropprioritetDataGridViewTextBoxColumn";
            // 
            // gjenstandDataGridViewTextBoxColumn
            // 
            this.gjenstandDataGridViewTextBoxColumn.DataPropertyName = "gjenstand";
            this.gjenstandDataGridViewTextBoxColumn.HeaderText = "gjenstand";
            this.gjenstandDataGridViewTextBoxColumn.Name = "gjenstandDataGridViewTextBoxColumn";
            // 
            // fakturaDataGridViewTextBoxColumn
            // 
            this.fakturaDataGridViewTextBoxColumn.DataPropertyName = "faktura";
            this.fakturaDataGridViewTextBoxColumn.HeaderText = "faktura";
            this.fakturaDataGridViewTextBoxColumn.Name = "fakturaDataGridViewTextBoxColumn";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 278);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Kjøpte gjenstander";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 556);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Solgte  gjenstander";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_forste_medlem);
            this.flowLayoutPanel1.Controls.Add(this.btn_forrige_medlem);
            this.flowLayoutPanel1.Controls.Add(this.tb_current_medlem);
            this.flowLayoutPanel1.Controls.Add(this.btn_neste_medlem);
            this.flowLayoutPanel1.Controls.Add(this.btn_siste_medlem);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(712, 235);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(253, 26);
            this.flowLayoutPanel1.TabIndex = 49;
            // 
            // btn_forste_medlem
            // 
            this.btn_forste_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_forste_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_forste_medlem.Location = new System.Drawing.Point(3, 3);
            this.btn_forste_medlem.Name = "btn_forste_medlem";
            this.btn_forste_medlem.Size = new System.Drawing.Size(25, 23);
            this.btn_forste_medlem.TabIndex = 48;
            this.btn_forste_medlem.Text = "|<";
            this.btn_forste_medlem.UseVisualStyleBackColor = true;
            this.btn_forste_medlem.Click += new System.EventHandler(this.btn_forste_medlem_Click);
            // 
            // btn_forrige_medlem
            // 
            this.btn_forrige_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_forrige_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_forrige_medlem.Location = new System.Drawing.Point(34, 3);
            this.btn_forrige_medlem.Name = "btn_forrige_medlem";
            this.btn_forrige_medlem.Size = new System.Drawing.Size(23, 23);
            this.btn_forrige_medlem.TabIndex = 44;
            this.btn_forrige_medlem.Text = "<";
            this.btn_forrige_medlem.UseVisualStyleBackColor = true;
            this.btn_forrige_medlem.Click += new System.EventHandler(this.btn_forrige_medlem_Click);
            // 
            // tb_current_medlem
            // 
            this.tb_current_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_current_medlem.Location = new System.Drawing.Point(63, 4);
            this.tb_current_medlem.Name = "tb_current_medlem";
            this.tb_current_medlem.Size = new System.Drawing.Size(55, 20);
            this.tb_current_medlem.TabIndex = 50;
            this.tb_current_medlem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_current_medlem_KeyPress);
            this.tb_current_medlem.Leave += new System.EventHandler(this.tb_current_medlem_Leave);
            this.tb_current_medlem.Validated += new System.EventHandler(this.tb_current_medlem_Leave);
            // 
            // btn_neste_medlem
            // 
            this.btn_neste_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_neste_medlem.AutoSize = true;
            this.btn_neste_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_neste_medlem.Location = new System.Drawing.Point(124, 3);
            this.btn_neste_medlem.Name = "btn_neste_medlem";
            this.btn_neste_medlem.Size = new System.Drawing.Size(25, 23);
            this.btn_neste_medlem.TabIndex = 46;
            this.btn_neste_medlem.Text = ">";
            this.btn_neste_medlem.UseVisualStyleBackColor = true;
            this.btn_neste_medlem.Click += new System.EventHandler(this.btn_neste_medlem_Click);
            // 
            // btn_siste_medlem
            // 
            this.btn_siste_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_siste_medlem.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_siste_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_siste_medlem.Location = new System.Drawing.Point(155, 3);
            this.btn_siste_medlem.Name = "btn_siste_medlem";
            this.btn_siste_medlem.Size = new System.Drawing.Size(25, 23);
            this.btn_siste_medlem.TabIndex = 47;
            this.btn_siste_medlem.Text = ">|";
            this.btn_siste_medlem.UseVisualStyleBackColor = true;
            this.btn_siste_medlem.Click += new System.EventHandler(this.btn_siste_medlem_Click);
            // 
            // oppgjorTableLayoutPanel
            // 
            this.oppgjorTableLayoutPanel.ColumnCount = 4;
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52F));
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48F));
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.oppgjorTableLayoutPanel.Controls.Add(this.sumTB, 3, 4);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label7, 0, 4);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label5, 0, 0);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label2, 0, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label1, 1, 1);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label3, 2, 1);
            this.oppgjorTableLayoutPanel.Controls.Add(this.nettoKjopTB, 1, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label6, 3, 1);
            this.oppgjorTableLayoutPanel.Controls.Add(this.provisjonKjopTB, 2, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.bruttoKjopTB, 3, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.nettoSalgTB, 1, 2);
            this.oppgjorTableLayoutPanel.Controls.Add(this.bruttoSalgTB, 3, 2);
            this.oppgjorTableLayoutPanel.Controls.Add(this.provisjonSalgTB, 2, 2);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label4, 0, 2);
            this.oppgjorTableLayoutPanel.Controls.Add(this.btn_beregn_oppgjor, 0, 5);
            this.oppgjorTableLayoutPanel.Controls.Add(this.btn_produser_faktura, 2, 5);
            this.oppgjorTableLayoutPanel.Location = new System.Drawing.Point(712, 294);
            this.oppgjorTableLayoutPanel.Name = "oppgjorTableLayoutPanel";
            this.oppgjorTableLayoutPanel.RowCount = 6;
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.35714F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.64286F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.oppgjorTableLayoutPanel.Size = new System.Drawing.Size(402, 198);
            this.oppgjorTableLayoutPanel.TabIndex = 42;
            // 
            // sumTB
            // 
            this.sumTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sumTB.Location = new System.Drawing.Point(305, 127);
            this.sumTB.Name = "sumTB";
            this.sumTB.Size = new System.Drawing.Size(94, 20);
            this.sumTB.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Sum";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Oppgjør";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Kjøp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(107, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Netto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(203, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Provisjoner";
            // 
            // nettoKjopTB
            // 
            this.nettoKjopTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nettoKjopTB.Location = new System.Drawing.Point(107, 91);
            this.nettoKjopTB.Name = "nettoKjopTB";
            this.nettoKjopTB.Size = new System.Drawing.Size(90, 20);
            this.nettoKjopTB.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(305, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Brutto";
            // 
            // provisjonKjopTB
            // 
            this.provisjonKjopTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.provisjonKjopTB.Location = new System.Drawing.Point(203, 91);
            this.provisjonKjopTB.Name = "provisjonKjopTB";
            this.provisjonKjopTB.Size = new System.Drawing.Size(96, 20);
            this.provisjonKjopTB.TabIndex = 12;
            // 
            // bruttoKjopTB
            // 
            this.bruttoKjopTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bruttoKjopTB.Location = new System.Drawing.Point(305, 91);
            this.bruttoKjopTB.Name = "bruttoKjopTB";
            this.bruttoKjopTB.Size = new System.Drawing.Size(94, 20);
            this.bruttoKjopTB.TabIndex = 16;
            // 
            // nettoSalgTB
            // 
            this.nettoSalgTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nettoSalgTB.Location = new System.Drawing.Point(107, 59);
            this.nettoSalgTB.Name = "nettoSalgTB";
            this.nettoSalgTB.Size = new System.Drawing.Size(90, 20);
            this.nettoSalgTB.TabIndex = 6;
            // 
            // bruttoSalgTB
            // 
            this.bruttoSalgTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bruttoSalgTB.Location = new System.Drawing.Point(305, 59);
            this.bruttoSalgTB.Name = "bruttoSalgTB";
            this.bruttoSalgTB.Size = new System.Drawing.Size(94, 20);
            this.bruttoSalgTB.TabIndex = 14;
            // 
            // provisjonSalgTB
            // 
            this.provisjonSalgTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.provisjonSalgTB.Location = new System.Drawing.Point(203, 59);
            this.provisjonSalgTB.Name = "provisjonSalgTB";
            this.provisjonSalgTB.Size = new System.Drawing.Size(96, 20);
            this.provisjonSalgTB.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Salg";
            // 
            // btn_beregn_oppgjor
            // 
            this.oppgjorTableLayoutPanel.SetColumnSpan(this.btn_beregn_oppgjor, 2);
            this.btn_beregn_oppgjor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_beregn_oppgjor.Location = new System.Drawing.Point(3, 160);
            this.btn_beregn_oppgjor.Name = "btn_beregn_oppgjor";
            this.btn_beregn_oppgjor.Size = new System.Drawing.Size(194, 35);
            this.btn_beregn_oppgjor.TabIndex = 19;
            this.btn_beregn_oppgjor.Text = "Beregn";
            this.btn_beregn_oppgjor.UseVisualStyleBackColor = true;
            this.btn_beregn_oppgjor.Click += new System.EventHandler(this.btn_beregn_oppgjor_Click);
            // 
            // btn_produser_faktura
            // 
            this.oppgjorTableLayoutPanel.SetColumnSpan(this.btn_produser_faktura, 2);
            this.btn_produser_faktura.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_produser_faktura.Location = new System.Drawing.Point(203, 160);
            this.btn_produser_faktura.Name = "btn_produser_faktura";
            this.btn_produser_faktura.Size = new System.Drawing.Size(196, 35);
            this.btn_produser_faktura.TabIndex = 20;
            this.btn_produser_faktura.Text = "Faktura";
            this.btn_produser_faktura.UseVisualStyleBackColor = true;
            this.btn_produser_faktura.Click += new System.EventHandler(this.btn_produser_faktura_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.Controls.Add(this.label10, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label17, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(712, 23);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57.37705F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.62295F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(403, 195);
            this.tableLayoutPanel1.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "mellomnavn", true));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(194, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 17);
            this.label10.TabIndex = 40;
            this.label10.Text = "label10";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Medlem";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "fornavn", true));
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "land", true));
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 17);
            this.label12.TabIndex = 39;
            this.label12.Text = "label12";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label13, 2);
            this.label13.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "etternavn", true));
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 17);
            this.label13.TabIndex = 34;
            this.label13.Text = "label13";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "postnummer", true));
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 17);
            this.label16.TabIndex = 37;
            this.label16.Text = "label16";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "gate1", true));
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 17);
            this.label14.TabIndex = 35;
            this.label14.Text = "label14";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "gate2", true));
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(194, 101);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 17);
            this.label15.TabIndex = 36;
            this.label15.Text = "label15";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.medlemBindingSource, "poststed", true));
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(194, 133);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 17);
            this.label17.TabIndex = 38;
            this.label17.Text = "label17";
            // 
            // solgtVarelinjerDGV
            // 
            this.solgtVarelinjerDGV.AllowUserToAddRows = false;
            this.solgtVarelinjerDGV.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.solgtVarelinjerDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.solgtVarelinjerDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.solgtVarelinjerDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.solgtVarelinjerDGV.DefaultCellStyle = dataGridViewCellStyle3;
            this.solgtVarelinjerDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.solgtVarelinjerDGV.Location = new System.Drawing.Point(3, 572);
            this.solgtVarelinjerDGV.Name = "solgtVarelinjerDGV";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.solgtVarelinjerDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.solgtVarelinjerDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.solgtVarelinjerDGV.Size = new System.Drawing.Size(695, 224);
            this.solgtVarelinjerDGV.TabIndex = 21;
            // 
            // kjoperDGV
            // 
            this.kjoperDGV.AllowUserToAddRows = false;
            this.kjoperDGV.AllowUserToDeleteRows = false;
            this.kjoperDGV.AutoGenerateColumns = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kjoperDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.kjoperDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.kjoperDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.medlemnummerDataGridViewTextBoxColumn,
            this.fornavnDataGridViewTextBoxColumn,
            this.mellomnavnDataGridViewTextBoxColumn,
            this.etternavnDataGridViewTextBoxColumn,
            this.gate1DataGridViewTextBoxColumn,
            this.gate2DataGridViewTextBoxColumn,
            this.postnummerDataGridViewTextBoxColumn,
            this.poststedDataGridViewTextBoxColumn,
            this.landDataGridViewTextBoxColumn});
            this.kjoperDGV.DataSource = this.medlemBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.kjoperDGV.DefaultCellStyle = dataGridViewCellStyle6;
            this.kjoperDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kjoperDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.kjoperDGV.Location = new System.Drawing.Point(3, 23);
            this.kjoperDGV.MultiSelect = false;
            this.kjoperDGV.Name = "kjoperDGV";
            this.kjoperDGV.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kjoperDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.tableLayoutPanel2.SetRowSpan(this.kjoperDGV, 2);
            this.kjoperDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kjoperDGV.Size = new System.Drawing.Size(695, 238);
            this.kjoperDGV.TabIndex = 0;
            this.kjoperDGV.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.kjoperDGV_RowEnter);
            // 
            // kjoptVarelinjerDGV
            // 
            this.kjoptVarelinjerDGV.AllowUserToAddRows = false;
            this.kjoptVarelinjerDGV.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.kjoptVarelinjerDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kjoptVarelinjerDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.kjoptVarelinjerDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.kjoptVarelinjerDGV.DefaultCellStyle = dataGridViewCellStyle10;
            this.kjoptVarelinjerDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kjoptVarelinjerDGV.Location = new System.Drawing.Point(3, 294);
            this.kjoptVarelinjerDGV.Name = "kjoptVarelinjerDGV";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kjoptVarelinjerDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.kjoptVarelinjerDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kjoptVarelinjerDGV.Size = new System.Drawing.Size(695, 244);
            this.kjoptVarelinjerDGV.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.20264F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.79736F));
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.oppgjorTableLayoutPanel, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.kjoptVarelinjerDGV, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.solgtVarelinjerDGV, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.kjoperDGV, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.button1, 2, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.74598F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.25402F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 230F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1135, 820);
            this.tableLayoutPanel2.TabIndex = 50;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(712, 544);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 22);
            this.button1.TabIndex = 50;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // medlemnummerDataGridViewTextBoxColumn
            // 
            this.medlemnummerDataGridViewTextBoxColumn.DataPropertyName = "medlem_nummer";
            this.medlemnummerDataGridViewTextBoxColumn.HeaderText = "medlem_nummer";
            this.medlemnummerDataGridViewTextBoxColumn.Name = "medlemnummerDataGridViewTextBoxColumn";
            this.medlemnummerDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fornavnDataGridViewTextBoxColumn
            // 
            this.fornavnDataGridViewTextBoxColumn.DataPropertyName = "fornavn";
            this.fornavnDataGridViewTextBoxColumn.HeaderText = "fornavn";
            this.fornavnDataGridViewTextBoxColumn.Name = "fornavnDataGridViewTextBoxColumn";
            this.fornavnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mellomnavnDataGridViewTextBoxColumn
            // 
            this.mellomnavnDataGridViewTextBoxColumn.DataPropertyName = "mellomnavn";
            this.mellomnavnDataGridViewTextBoxColumn.HeaderText = "mellomnavn";
            this.mellomnavnDataGridViewTextBoxColumn.Name = "mellomnavnDataGridViewTextBoxColumn";
            this.mellomnavnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // etternavnDataGridViewTextBoxColumn
            // 
            this.etternavnDataGridViewTextBoxColumn.DataPropertyName = "etternavn";
            this.etternavnDataGridViewTextBoxColumn.HeaderText = "etternavn";
            this.etternavnDataGridViewTextBoxColumn.Name = "etternavnDataGridViewTextBoxColumn";
            this.etternavnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gate1DataGridViewTextBoxColumn
            // 
            this.gate1DataGridViewTextBoxColumn.DataPropertyName = "gate1";
            this.gate1DataGridViewTextBoxColumn.HeaderText = "gate1";
            this.gate1DataGridViewTextBoxColumn.Name = "gate1DataGridViewTextBoxColumn";
            this.gate1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gate2DataGridViewTextBoxColumn
            // 
            this.gate2DataGridViewTextBoxColumn.DataPropertyName = "gate2";
            this.gate2DataGridViewTextBoxColumn.HeaderText = "gate2";
            this.gate2DataGridViewTextBoxColumn.Name = "gate2DataGridViewTextBoxColumn";
            this.gate2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postnummerDataGridViewTextBoxColumn
            // 
            this.postnummerDataGridViewTextBoxColumn.DataPropertyName = "postnummer";
            this.postnummerDataGridViewTextBoxColumn.HeaderText = "postnummer";
            this.postnummerDataGridViewTextBoxColumn.Name = "postnummerDataGridViewTextBoxColumn";
            this.postnummerDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // poststedDataGridViewTextBoxColumn
            // 
            this.poststedDataGridViewTextBoxColumn.DataPropertyName = "poststed";
            this.poststedDataGridViewTextBoxColumn.HeaderText = "poststed";
            this.poststedDataGridViewTextBoxColumn.Name = "poststedDataGridViewTextBoxColumn";
            this.poststedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // landDataGridViewTextBoxColumn
            // 
            this.landDataGridViewTextBoxColumn.DataPropertyName = "land";
            this.landDataGridViewTextBoxColumn.HeaderText = "land";
            this.landDataGridViewTextBoxColumn.Name = "landDataGridViewTextBoxColumn";
            this.landDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // medlemBindingSource
            // 
            this.medlemBindingSource.DataSource = typeof(auksjonDataAksessLib.medlem);
            this.medlemBindingSource.CurrentChanged += new System.EventHandler(this.medlemBindingSource_CurrentChanged);
            // 
            // varelinjeBindingSource
            // 
            this.varelinjeBindingSource.DataSource = this.medlemBindingSource.DataSource;
            // 
            // auksjonFaktura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 820);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "auksjonFaktura";
            this.Text = "Auksjon faktura";
            this.Load += new System.EventHandler(this.auksjonFaktura_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.oppgjorTableLayoutPanel.ResumeLayout(false);
            this.oppgjorTableLayoutPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.solgtVarelinjerDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kjoperDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kjoptVarelinjerDGV)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medlemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.varelinjeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource medlemBindingSource;
        private System.Windows.Forms.BindingSource varelinjeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn medlemidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontonummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn epostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxgjenstanderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn utropprioritetDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gjenstandDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fakturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btn_forste_medlem;
        private System.Windows.Forms.Button btn_forrige_medlem;
        private System.Windows.Forms.TextBox tb_current_medlem;
        private System.Windows.Forms.Button btn_neste_medlem;
        private System.Windows.Forms.Button btn_siste_medlem;
        private System.Windows.Forms.TableLayoutPanel oppgjorTableLayoutPanel;
        private System.Windows.Forms.TextBox sumTB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nettoKjopTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox provisjonKjopTB;
        private System.Windows.Forms.TextBox bruttoKjopTB;
        private System.Windows.Forms.TextBox nettoSalgTB;
        private System.Windows.Forms.TextBox bruttoSalgTB;
        private System.Windows.Forms.TextBox provisjonSalgTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_beregn_oppgjor;
        private System.Windows.Forms.Button btn_produser_faktura;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView solgtVarelinjerDGV;
        private System.Windows.Forms.DataGridView kjoperDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn medlemnummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fornavnDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mellomnavnDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn etternavnDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gate1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gate2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postnummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn poststedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn landDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView kjoptVarelinjerDGV;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button1;

    }
}

