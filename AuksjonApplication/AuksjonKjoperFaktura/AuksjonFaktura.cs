﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonKjoperFaktura
{
    public partial class auksjonFaktura : Form
    {
        AuksjonDataClassesDataContext db;
        IEnumerable<varelinje_grunnlag> kjoptVarelinjer_;
        IEnumerable<varelinje_grunnlag> solgtVarelinjer_;
        auksjon denne_auksjon_;
        List<varelinje_grunnlag> salg_til_faktura;
        List<varelinje_grunnlag> kjop_til_faktura;
        faktura_grunnlag ny_faktura_grunnlag;
        int denne_auksjon_id;
    
        public auksjonFaktura()
        {
            InitializeComponent();
            salg_til_faktura = new List<varelinje_grunnlag>();
            kjop_til_faktura = new List<varelinje_grunnlag>();
            ny_faktura_grunnlag = new faktura_grunnlag();
        }

        public void init_kjoperdgv()
        {
            kjoperDGV.Columns["Adresse1"].Visible = false;
        }

        private void orderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

        }

        private void auksjonFaktura_Load(object sender, EventArgs e)
        {
            // create new data context
            db = new AuksjonDataClassesDataContext();
            denne_auksjon_id = db.app_config_datas.Single().current_auksjon_id;

            denne_auksjon_ = db.auksjons.Single( p => p.ID == denne_auksjon_id);

            // set the binding source data source to the full order table
            this.medlemBindingSource.DataSource = db.GetTable<medlem_for_faktura_view>();

            //tb_current_medlem.DataBindings.Add("Text", medlemBindingSource, "Position");
        }

        private void kjoperDGV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            int medlem_nr = (int)dgv[0, e.RowIndex].Value;
            medlem medl = db.medlems.Single(p => p.medlem_nummer == medlem_nr);
            kjoptVarelinjer_ = from gj_vare in db.gjenstands
                                where gj_vare.kjoper_nummer == medl.medlem_nummer && gj_vare.auksjon_id==denne_auksjon_id
                                select new varelinje_grunnlag
                                {
                                    gjenstand_id = gj_vare.ID,
                                    utrop_nr = gj_vare.utrop_nummer.GetValueOrDefault(0),
                                    navn = gj_vare.navn,
                                    selger_id = gj_vare.selger_id.GetValueOrDefault(0),
                                    kjoper_id = gj_vare.kjoper_id.GetValueOrDefault(0),
                                    netto = (float)gj_vare.pris.GetValueOrDefault(0),
                                    minstepris = (float)gj_vare.minstepris.GetValueOrDefault(0),
                                    utropt = gj_vare.utropt ?? false,
                                    fakturert = gj_vare.fakturert_kjoper ?? false,
                                    provisjon = (beregn_kjoper_provisjon((float)gj_vare.pris.GetValueOrDefault(0), (float)gj_vare.minstepris.GetValueOrDefault())),
                                    fakturer_denne = ((gj_vare.fakturert_kjoper ?? false)) == false && ((gj_vare.utropt ?? false) == true)
                                };

            solgtVarelinjer_ = from gj_vare in db.gjenstands
                               where gj_vare.selger_id == medl.medlem_nummer && gj_vare.auksjon_id == denne_auksjon_id
                                select new varelinje_grunnlag
                                {
                                    gjenstand_id = gj_vare.ID,
                                    utrop_nr = gj_vare.utrop_nummer.GetValueOrDefault(0),
                                    navn = gj_vare.navn,
                                    selger_id = gj_vare.selger_id.GetValueOrDefault(0),
                                    kjoper_id = gj_vare.kjoper_id.GetValueOrDefault(0),
                                    netto = (float)gj_vare.pris.GetValueOrDefault(0),
                                    minstepris = (float)gj_vare.minstepris.GetValueOrDefault(0),
                                    utropt = gj_vare.utropt ?? false,
                                    fakturert = gj_vare.fakturert_selger ?? false,
                                    provisjon = beregn_selger_provisjon((float)gj_vare.pris.GetValueOrDefault(0), (float)gj_vare.minstepris.GetValueOrDefault()),
                                    fakturer_denne = ((gj_vare.fakturert_selger ?? false) == false) && ((gj_vare.utropt ?? false) == true) 
                                };
            kjoptVarelinjerDGV.DataSource = kjoptVarelinjer_;
            kjoptVarelinjerDGV.AutoResizeColumns();
            solgtVarelinjerDGV.DataSource = solgtVarelinjer_;
            solgtVarelinjerDGV.AutoResizeColumns();
        }

        private void lagFaktura_Click(object sender, EventArgs e)
        {
        }

        private void beregn_faktura_grunnlag(object sender, EventArgs e)
        {
            int medlem_nr = (int)kjoperDGV.CurrentRow.Cells[0].Value;
            medlem denne_medl = db.medlems.SingleOrDefault(p => p.medlem_nummer == medlem_nr);
            int medlem_id = denne_medl.ID;
            //Insert faktura
            //IEnumerable<gjenstand> varelinjer = dc.ExecuteQuery<gjenstand>(@"SELECT * from ")
            //gjenstands.Select(p => (p.kjoper_id == medlem_id || p.selger_id == medlem_id));
//            gjenstandDb.gjenstands.InsertOnSubmit(enGjenstand);
            
            float netto_salg = 0;
            float netto_kjop = 0;
            float provisjon_salg = 0;
            float provisjon_kjop = 0;
            float brutto_salg = 0;
            float brutto_kjop = 0;
            float netto = 0;

            List<varelinje_grunnlag> solgt_faktura_rader = new List<varelinje_grunnlag>();
            List<varelinje_grunnlag> kjopt_faktura_rader = new List<varelinje_grunnlag>();

            //Beregn provisjoner og netto
            kjop_til_faktura.Clear();
            int row_ix = 0;
            foreach (varelinje_grunnlag linje in kjoptVarelinjer_)
            {
                varelinje_grunnlag l = new varelinje_grunnlag();
                l = linje;
                if (true == (bool)kjoptVarelinjerDGV.Rows[row_ix].Cells["fakturer_denne"].EditedFormattedValue)
                {
                    l.provisjon = beregn_provisjoner(l, medlem_nr);
                    kjopt_faktura_rader.Add(l);
                    //netto_salg += l.netto_salg;
                    netto_kjop += l.netto;
                    //provisjon_salg += l.provisjon_salg;
                    provisjon_kjop += l.provisjon;
                    kjop_til_faktura.Add(l);
                }
                ++row_ix;
            }

            salg_til_faktura.Clear();
            row_ix = 0;
            foreach (varelinje_grunnlag linje in solgtVarelinjer_)
            {
                varelinje_grunnlag l = new varelinje_grunnlag();
                l = linje;
                DataGridViewElementStates iii = solgtVarelinjerDGV.Rows[row_ix].Cells["fakturer_denne"].State;
                string sss = solgtVarelinjerDGV.Rows[row_ix].Cells["fakturer_denne"].Value.ToString();
                if (true == (bool)solgtVarelinjerDGV.Rows[row_ix].Cells["fakturer_denne"].EditedFormattedValue)
                {
                    l.provisjon = beregn_provisjoner(l, medlem_nr);
                    solgt_faktura_rader.Add(l);
                    netto_salg += l.netto;
                    //netto_kjop += l.netto_kjop;
                    provisjon_salg += l.provisjon;
                    //provisjon_kjop += l.provisjon_kjop;
                    salg_til_faktura.Add(l);
                }
                ++row_ix;
            }

            brutto_salg = netto_salg - provisjon_salg;
            brutto_kjop = netto_kjop + provisjon_kjop;
            netto = brutto_salg - brutto_kjop;

            nettoSalgTB.Text = netto_salg.ToString();
            nettoKjopTB.Text = netto_kjop.ToString();
            provisjonSalgTB.Text = provisjon_salg.ToString();
            provisjonKjopTB.Text = provisjon_kjop.ToString();
            bruttoSalgTB.Text = brutto_salg.ToString();
            bruttoKjopTB.Text = (0 - brutto_kjop).ToString();
            sumTB.Text = netto.ToString();

            ny_faktura_grunnlag.medlem_id = medlem_id;
            ny_faktura_grunnlag.sum = netto;
            //Insert varelinjer to database
            //kjoptVarelinjerDGV.DataSource = kjoptVarelinjer;// kjopt_faktura_rader;
            //solgtVarelinjerDGV.DataSource = solgtVarelinjer;// solgt_faktura_rader;
        }

        private int beregn_selger_provisjon(float pris, float minstepris)
        {
            float provisjon;
            if (pris == 0)
            {
                provisjon = (minstepris * denne_auksjon_.provisjon_usolgt ?? 0);
                if (provisjon != 0)
                    provisjon /= 100;
                if (provisjon < (float)denne_auksjon_.provisjon_usolgt_min)
                    provisjon = (float)denne_auksjon_.provisjon_usolgt_min;
                if (provisjon > (float)denne_auksjon_.provisjon_usolgt_max)
                    provisjon = (float)denne_auksjon_.provisjon_usolgt_max;
            }
            else
            {
                provisjon = pris * denne_auksjon_.provisjon_salg ?? 0;
                if (provisjon != 0)
                    provisjon /= 100;
            }
            return (int)Math.Ceiling(provisjon);
        }

        private int beregn_kjoper_provisjon(float pris, float minstepris)
        {
            float provisjon;
            provisjon = pris * denne_auksjon_.provisjon_kjop ?? 0;
            if (provisjon != 0)
                provisjon /= 100;

            return (int)Math.Ceiling(provisjon);
        }

        private int beregn_provisjoner(varelinje_grunnlag varelinje, int medlem_nr)
        {
            int selger_sats = (int)denne_auksjon_.provisjon_salg;
            int kjoper_sats = (int)denne_auksjon_.provisjon_kjop;
            int usolgt_sats = (int)denne_auksjon_.provisjon_usolgt;
            float usolgt_min_sats = (float)denne_auksjon_.provisjon_usolgt_min;
            float usolgt_max_sats = (float)denne_auksjon_.provisjon_usolgt_max;
            float provisjon = 0;

            if (medlem_nr == varelinje.selger_id)    //selger
            {
                provisjon = beregn_selger_provisjon(varelinje.netto, varelinje.minstepris);
            }
            else
            {                                   //kjøper
                provisjon = beregn_kjoper_provisjon(varelinje.netto, kjoper_sats);
            }

            return (int)Math.Ceiling(provisjon);
        }


        //private int beregn_provisjoner(varelinje_grunnlag varelinje, int medlem_nr)
        //{
        //    int selger_sats = (int)denne_auksjon_.provisjon_salg;
        //    int kjoper_sats = (int)denne_auksjon_.provisjon_kjop;
        //    int usolgt_sats = (int)denne_auksjon_.provisjon_usolgt;
        //    float usolgt_min_sats = (float)denne_auksjon_.provisjon_usolgt_min;
        //    float usolgt_max_sats = (float)denne_auksjon_.provisjon_usolgt_max;
        //    decimal provisjon = 0;

        //    if (medlem_nr == varelinje.selger_id)    //selger
        //    {
        //        provisjon = varelinje.netto * selger_sats; //provisjon ved omsetning
        //        provisjon /= 100;

        //        if (varelinje.netto == 0)                                      //provisjon usolgt
        //        {
        //            provisjon = varelinje.minstepris * usolgt_sats;
        //            provisjon /= 100;
        //            if (provisjon < usolgt_min_sats)
        //                provisjon = usolgt_min_sats;
        //            else if (provisjon > usolgt_max_sats)
        //                provisjon = usolgt_max_sats;
        //        }
        //    }
        //    else
        //    {                                   //kjøper
        //        provisjon = varelinje.netto * kjoper_sats;
        //        provisjon /= 100;
        //    }

        //    return (int)Math.Ceiling(varelinje.provisjon);
        //}

        private void toolStripFaktura_Click(object sender, EventArgs e)
        {
            beregn_faktura_grunnlag(sender, e);
        }

        private void lagre_faktura()
        {
            string faktura_folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
            string faktura_medl_id_ = "1-123";
            string file_name = string.Concat(faktura_medl_id_, ".pdf");
            faktura_folder = System.IO.Path.Combine(faktura_folder, "faktura");
            faktura_folder = System.IO.Path.Combine(faktura_folder, file_name);
        }

        private void toolStripVisFaktura_Click(object sender, EventArgs e)
        {
            lagre_faktura();
            try         //Legg til varelinjer i faktura tabell
            {
                faktura ny_faktura = new faktura();
                ny_faktura.sum = (decimal)ny_faktura_grunnlag.sum;
                ny_faktura.medlem_id = ny_faktura_grunnlag.medlem_id;
                ny_faktura.faktura_dato = DateTime.Now.Date;
                ny_faktura.auksjon_id = denne_auksjon_id;
                db.fakturas.InsertOnSubmit(ny_faktura);
                db.SubmitChanges();

                if (kjop_til_faktura.Count > 0)
                {
                    foreach (varelinje_grunnlag l in kjop_til_faktura)
                    {
                        varelinje ny_linje = new varelinje();
                        ny_linje.ID = ny_faktura.ID;
                        ny_linje.utrop_nr = l.utrop_nr;
                        ny_linje.beskrivelse = l.navn;
                        ny_linje.gjenstand_id = l.gjenstand_id;
                        ny_linje.kjoper_id = l.kjoper_id;
                        ny_linje.selger_id = l.selger_id;
                        ny_linje.netto_kjop = (decimal)l.netto;
                        ny_linje.provisjon_kjop = (decimal)l.provisjon;
                        db.varelinjes.InsertOnSubmit(ny_linje);
                    }
                    db.SubmitChanges();
                }

                if (salg_til_faktura.Count > 0)
                {
                    foreach (varelinje_grunnlag l in salg_til_faktura)
                    {
                        varelinje ny_linje = new varelinje();
                        ny_linje.ID = ny_faktura.ID;
                        ny_linje.utrop_nr = l.utrop_nr;
                        ny_linje.gjenstand_id = l.gjenstand_id;
                        ny_linje.beskrivelse = l.navn;
                        ny_linje.kjoper_id = l.kjoper_id;
                        ny_linje.selger_id = l.selger_id;
                        ny_linje.netto_salg = (decimal)l.netto;
                        ny_linje.provisjon_salg = (decimal)l.provisjon;
                        db.varelinjes.InsertOnSubmit(ny_linje);
                    }
                    db.SubmitChanges();
                }
                
                //Sett fakturert_kjoper og fakturert selger
                foreach(varelinje_grunnlag kj_vgr in kjoptVarelinjer_)
                {
                    if (kj_vgr.fakturer_denne)
                    {
                        gjenstand kj_oppdat = db.gjenstands.Single(p => p.ID== kj_vgr.gjenstand_id);
                        kj_oppdat.fakturert_kjoper = true;
                        db.SubmitChanges();
                    }
                }

                foreach (varelinje_grunnlag solgt_vgr in solgtVarelinjer_)
                {
                    if (solgt_vgr.fakturer_denne)
                    {
                        gjenstand solgt_oppdat = db.gjenstands.Single(p => p.ID== solgt_vgr.gjenstand_id);
                        solgt_oppdat.fakturert_selger = true;
                        db.SubmitChanges();
                    }
                }
                
                //string msg = "Faktura " + ny_faktura.ID + " ble opprettet";
                //MessageBox.Show(msg);
               FakturaVisning fakt = new FakturaVisning(ny_faktura.ID);
                
                fakt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void medlemBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            tb_current_medlem.Text = ((auksjonDataAksessLib.medlem_for_faktura_view)(((BindingSource)sender).Current)).medlem_nummer.ToString();
        }
      
        private void btn_forrige_medlem_Click(object sender, EventArgs e)
        {
            medlemBindingSource.MovePrevious();
        }

        private void btn_neste_medlem_Click(object sender, EventArgs e)
        {
            medlemBindingSource.MoveNext();
        }

        private void btn_siste_medlem_Click(object sender, EventArgs e)
        {
            medlemBindingSource.MoveLast();
        }

        private void btn_forste_medlem_Click(object sender, EventArgs e)
        {
            medlemBindingSource.MoveFirst();
        }

        private void tb_current_medlem_Leave(object sender, EventArgs e)
        {
            //int  in_nummer = Int16.Parse(((TextBox)sender).Text);
            //int ny_id = db.medlems.Single(m => m.medlem_nummer == in_nummer).medlem_id;
            //medlemBindingSource.Position = ny_id-1;
        }

        private void tb_current_medlem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                int in_nummer = Int16.Parse(((TextBox)sender).Text);
                try
                {
                    var ny_obj = db.medlems.Single(m => m.medlem_nummer == in_nummer);
                    int index = db.medlems.ToList().IndexOf(ny_obj);
                    medlemBindingSource.Position = index;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                e.Handled = true;
                base.OnKeyPress(e);
            }
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_beregn_oppgjor_Click(object sender, EventArgs e)
        {
            beregn_faktura_grunnlag(sender, e);
        }

        private void btn_produser_faktura_Click(object sender, EventArgs e)
        {
            lagre_faktura();
            try         //Legg til varelinjer i faktura tabell
            {
                faktura ny_faktura = new faktura();
                ny_faktura.sum = (decimal)ny_faktura_grunnlag.sum;
                ny_faktura.medlem_id = ny_faktura_grunnlag.medlem_id;
                ny_faktura.faktura_dato = DateTime.Now.Date;
                ny_faktura.auksjon_id = denne_auksjon_id;
                db.fakturas.InsertOnSubmit(ny_faktura);
                db.SubmitChanges();

                if (kjop_til_faktura.Count > 0)
                {
                    foreach (varelinje_grunnlag l in kjop_til_faktura)
                    {
                        varelinje ny_linje = new varelinje();
                        ny_linje.faktura_id = ny_faktura.ID;
                        ny_linje.utrop_nr = l.utrop_nr;
                        ny_linje.beskrivelse = l.navn;
                        ny_linje.gjenstand_id = l.gjenstand_id;
                        ny_linje.kjoper_id = l.kjoper_id;
                        ny_linje.selger_id = l.selger_id;
                        ny_linje.netto_kjop = (decimal)l.netto;
                        ny_linje.provisjon_kjop = (decimal)l.provisjon;
                        db.varelinjes.InsertOnSubmit(ny_linje);
                    }
                    db.SubmitChanges();
                }

                if (salg_til_faktura.Count > 0)
                {
                    foreach (varelinje_grunnlag l in salg_til_faktura)
                    {
                        varelinje ny_linje = new varelinje();
                        ny_linje.faktura_id = ny_faktura.ID;
                        ny_linje.utrop_nr = l.utrop_nr;
                        ny_linje.gjenstand_id = l.gjenstand_id;
                        ny_linje.beskrivelse = l.navn;
                        ny_linje.kjoper_id = l.kjoper_id;
                        ny_linje.selger_id = l.selger_id;
                        ny_linje.netto_salg = (decimal)l.netto;
                        ny_linje.provisjon_salg = (decimal)l.provisjon;
                        db.varelinjes.InsertOnSubmit(ny_linje);
                    }
                    db.SubmitChanges();
                }

                //Sett fakturert_kjoper og fakturert selger
                foreach (varelinje_grunnlag kj_vgr in kjoptVarelinjer_)
                {
                    if (kj_vgr.fakturer_denne)
                    {
                        gjenstand kj_oppdat = db.gjenstands.Single(p => p.ID== kj_vgr.gjenstand_id);
                        kj_oppdat.fakturert_kjoper = true;
                        db.SubmitChanges();
                    }
                }

                foreach (varelinje_grunnlag solgt_vgr in solgtVarelinjer_)
                {
                    if (solgt_vgr.fakturer_denne)
                    {
                        gjenstand solgt_oppdat = db.gjenstands.Single(p => p.ID== solgt_vgr.gjenstand_id);
                        solgt_oppdat.fakturert_selger = true;
                        db.SubmitChanges();
                    }
                }

                FakturaVisning fakt = new FakturaVisning(ny_faktura.ID);
                fakt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            deltagere_oppgjor opg = new deltagere_oppgjor();
            opg.Show();
        }
    }

    class varelinje_grunnlag
    {
        public int ID { get; set; }
        public int gjenstand_id { get; set; }
        public int utrop_nr { get; set; }
        public string navn { get; set; }
        public int selger_id { get; set; }
        public int kjoper_id { get; set; }
        public float netto { get; set; }
        public int provisjon { get; set; }
        public float minstepris { get; set; }
        public bool utropt { get; set; }
        public bool fakturert { get; set; }
        public bool fakturer_denne { get; set; }
    }

    class faktura_grunnlag
    {
        public int medlem_id;
        public float sum;
        public bool behandlet;
    }

    class live_auksjon
    {
        public live_auksjon(int selger_sats, int kjoper_sats, int usolgt_sats, float usolgt_min_sats, float usolgt_max_sats)
        {
            selger_provisjon_sats = selger_sats;
            kjoper_provisjon_sats = kjoper_sats;
            usolgt_provisjon_sats = usolgt_sats;
            usolgt_provisjon_min_sats = usolgt_min_sats;
            usolgt_provisjon_max_sats = usolgt_max_sats;
        }
        
        public int ID { get; set; }
        public DateTime dato { get; set; }
        public int selger_provisjon_sats { get; set; }
        public int kjoper_provisjon_sats { get; set; }
        public int usolgt_provisjon_sats { get; set; }
        public float usolgt_provisjon_min_sats { get; set; }
        public float usolgt_provisjon_max_sats { get; set; }
    }
}
