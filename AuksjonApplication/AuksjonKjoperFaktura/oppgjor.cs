﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;
using System.Configuration;

namespace AuksjonKjoperFaktura
{
    public partial class Oppgjor : Form
    {
        AuksjonDataClassesDataContext db;
        IEnumerable<varelinje_grunnlag> _kjoptVarelinjer;
        IEnumerable<varelinje_grunnlag> _solgtVarelinjer;
        auksjon denne_auksjon_;
        readonly List<varelinje_grunnlag> _salgTilFaktura;
        readonly List<varelinje_grunnlag> _kjopTilFaktura;
        readonly faktura_grunnlag _nyFakturaGrunnlag;
        private int auksjonId;

        private readonly BindingSource _medlemBindingSource;
/*
        private BindingSource _varelinjeBindingSource;
*/
  
        //public oppgjor()
        //{
        //    kjop_til_faktura = new List<varelinje_grunnlag>();
        //    ny_faktura_grunnlag = new faktura_grunnlag();
        //    db = new AuksjonDataClassesDataContext();
        //    denne_auksjon_ = db.auksjons.Single(p => p.ID == denne_auksjon_id);
        //}

        public Oppgjor(ref BindingSource bs)
        {
            db = new AuksjonDataClassesDataContext();
            auksjonId = db.app_config_datas.First().current_auksjon_id;

            string conf = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AppSettingsSection appSettingSection = (AppSettingsSection)config.GetSection("applicationSettings");

            System.Collections.Specialized.NameValueCollection appSettings = ConfigurationManager.AppSettings;

           // denne_auksjon_id = int.Parse(ConfigurationManager.AppSettings["aktiv_auksjon"]);
            InitializeComponent();
            btn_produser_faktura.Enabled = false;

            _salgTilFaktura = new List<varelinje_grunnlag>();

            _medlemBindingSource = new System.Windows.Forms.BindingSource();
            _medlemBindingSource.DataSource = bs;
            _medlemBindingSource.Position = bs.Position;
            
            init_bindings();
            _salgTilFaktura = new List<varelinje_grunnlag>();
            _kjopTilFaktura = new List<varelinje_grunnlag>();
            _nyFakturaGrunnlag = new faktura_grunnlag();
            denne_auksjon_ = db.auksjons.Single(p => p.ID == auksjonId);

            load_grids_with_data();
            beregn_faktura_grunnlag();
        }

        private void init_bindings()
        {
            label10.DataBindings.Add(new Binding("Text", _medlemBindingSource, "mellomnavn", true));
            label11.DataBindings.Add(new Binding("Text", _medlemBindingSource, "fornavn", true));
            label12.DataBindings.Add(new Binding("Text", _medlemBindingSource, "land", true));
            label13.DataBindings.Add(new Binding("Text", _medlemBindingSource, "etternavn", true));
            label16.DataBindings.Add(new Binding("Text", _medlemBindingSource, "postnummer", true));
            label15.DataBindings.Add(new Binding("Text", _medlemBindingSource, "gate2", true));
            label17.DataBindings.Add(new Binding("Text", _medlemBindingSource, "poststed", true));
            label14.DataBindings.Add(new Binding("Text", _medlemBindingSource, "gate1", true));
        }

    private void load_grids_with_data()
        {
            int medlem_nr = ((Deltager)_medlemBindingSource.Current).Medlemnummer;
            medlem medl = db.medlems.Single(p => p.medlem_nummer == medlem_nr);
            _kjoptVarelinjer = (from gj_vare in db.gjenstands
                                where gj_vare.kjoper_nummer == medl.medlem_nummer && gj_vare.auksjon_id==auksjonId
                                select new varelinje_grunnlag
                                {
                                    gjenstand_id = gj_vare.ID,
                                    utrop_nr = gj_vare.utrop_nummer.GetValueOrDefault(0),
                                    navn = gj_vare.navn,
                                    selger_id = gj_vare.selger_id.GetValueOrDefault(0),
                                    kjoper_id = gj_vare.kjoper_id.GetValueOrDefault(0),
                                    netto = (float)gj_vare.pris.GetValueOrDefault(0),
                                    minstepris = (float)gj_vare.minstepris.GetValueOrDefault(0),
                                    utropt = gj_vare.utropt ?? false,
                                    fakturert = gj_vare.fakturert_kjoper ?? false,      //HUSK: provisjoner beregnes defered
                                    provisjon = (beregn_kjops_provisjon(gj_vare.pris.GetValueOrDefault(0), gj_vare.minstepris.GetValueOrDefault())),
                                    fakturer_denne = ((gj_vare.fakturert_kjoper ?? false)) == false && ((gj_vare.utropt ?? false) == true)
                                }).OrderByDescending(g => g.utrop_nr);

            _solgtVarelinjer = (from gj_vare in db.gjenstands
                               where gj_vare.selger_nummer == medl.medlem_nummer && gj_vare.auksjon_id == auksjonId
                                select new varelinje_grunnlag
                                {
                                    gjenstand_id = gj_vare.ID,
                                    utrop_nr = gj_vare.utrop_nummer.GetValueOrDefault(0),
                                    navn = gj_vare.navn,
                                    selger_id = gj_vare.selger_id.GetValueOrDefault(0),
                                    kjoper_id = gj_vare.kjoper_id.GetValueOrDefault(0),
                                    netto = (float)gj_vare.pris.GetValueOrDefault(0),
                                    minstepris = (float)gj_vare.minstepris.GetValueOrDefault(0),
                                    utropt = gj_vare.utropt ?? false,
                                    fakturert = gj_vare.fakturert_selger ?? false,          //HUSK: provisjoner beregnes defered
                                    provisjon = beregn_salgs_provisjon(gj_vare.pris.GetValueOrDefault(0), gj_vare.minstepris.GetValueOrDefault()),
                                    fakturer_denne = ((gj_vare.fakturert_selger ?? false) == false) && ((gj_vare.utropt ?? false) == true) 
                                }).OrderByDescending(g => g.utrop_nr);

            kjoptVarelinjerDGV.DataSource = _kjoptVarelinjer;
            kjoptVarelinjerDGV.Columns["ID"].Visible = false;
            kjoptVarelinjerDGV.Columns["gjenstand_id"].Visible = false;
            kjoptVarelinjerDGV.Columns["selger_id"].Visible = false;
            kjoptVarelinjerDGV.Columns["kjoper_id"].Visible = false;
            kjoptVarelinjerDGV.AutoResizeColumns();
            solgtVarelinjerDGV.DataSource = _solgtVarelinjer;
            solgtVarelinjerDGV.Columns["ID"].Visible = false;
            solgtVarelinjerDGV.Columns["gjenstand_id"].Visible = false;
            solgtVarelinjerDGV.Columns["selger_id"].Visible = false;
            solgtVarelinjerDGV.Columns["selger_id"].Visible = false;
            solgtVarelinjerDGV.AutoResizeColumns();
        }

    private int beregn_salgs_provisjon(decimal pris, decimal minstepris)
    {
        decimal provisjon;
        if (pris == 0)
        {
            provisjon = (minstepris * denne_auksjon_.provisjon_usolgt??0);
            if (provisjon != 0)
                provisjon /= 100;
            if (provisjon < denne_auksjon_.provisjon_usolgt_min)
                provisjon = denne_auksjon_.provisjon_usolgt_min ?? 0;
            if (provisjon > denne_auksjon_.provisjon_usolgt_max)
                provisjon = denne_auksjon_.provisjon_usolgt_max ?? 0;
        }
        else
        {
            provisjon = pris * denne_auksjon_.provisjon_salg ?? 0;
            if (provisjon != 0)
                provisjon /= 100;
        }
        return (int)Math.Round(provisjon, 0, MidpointRounding.AwayFromZero);
    }

    private int beregn_kjops_provisjon(decimal pris, decimal minstepris)
    {
        decimal provisjon;
        provisjon = pris * denne_auksjon_.provisjon_kjop ?? 0;
        if (provisjon != 0)
            provisjon /= 100;

        return (int)Math.Round(provisjon, 0, MidpointRounding.AwayFromZero);
    }

    private int beregn_provisjoner(varelinje_grunnlag linje, int medlem_nr)
    {
        int selger_sats = (int)denne_auksjon_.provisjon_salg;
        int kjoper_sats = (int)denne_auksjon_.provisjon_kjop;
        int usolgt_sats = (int)denne_auksjon_.provisjon_usolgt;
        float usolgt_min_sats = (float)denne_auksjon_.provisjon_usolgt_min;
        float usolgt_max_sats = (float)denne_auksjon_.provisjon_usolgt_max;
        double provisjon = 0.0;

        if (medlem_nr == linje.selger_id)    //selger
        {
            provisjon = linje.netto * selger_sats; //provisjon ved omsetning
            provisjon /= 100;

            if (linje.netto == 0)                                      //provisjon usolgt
            {
                provisjon = linje.minstepris * usolgt_sats;
                provisjon /= 100;
                if (provisjon < usolgt_min_sats)
                    provisjon = usolgt_min_sats;
                else if (provisjon > usolgt_max_sats)
                    provisjon = usolgt_max_sats;
            }
        }
        else
        {                                   //kjøper
            provisjon = linje.netto * kjoper_sats;
            provisjon /= 100;
        }

        return (int)Math.Round(provisjon, 0, MidpointRounding.AwayFromZero);
    }

    private void beregn_faktura_grunnlag_cb(object sender, EventArgs e)
    {
        beregn_faktura_grunnlag();
    }

    private void beregn_faktura_grunnlag()
    {
        int medlem_nr = ((Deltager)_medlemBindingSource.Current).Medlemnummer;
        medlem denne_medl = db.medlems.SingleOrDefault(p => p.medlem_nummer == medlem_nr);
        int medlem_id = denne_medl.ID;
        //Insert faktura
        //IEnumerable<gjenstand> varelinjer = dc.ExecuteQuery<gjenstand>(@"SELECT * from ")
        //gjenstands.Select(p => (p.kjoper_id == medlem_id || p.selger_id == medlem_id));
        //            gjenstandDb.gjenstands.InsertOnSubmit(enGjenstand);

        float netto_salg = 0;
        float netto_kjop = 0;
        float provisjon_salg = 0;
        float provisjon_kjop = 0;
        float brutto_salg = 0;
        float brutto_kjop = 0;
        float netto = 0;

        List<varelinje_grunnlag> solgt_faktura_rader = new List<varelinje_grunnlag>();
        List<varelinje_grunnlag> kjopt_faktura_rader = new List<varelinje_grunnlag>();

        //Beregn provisjoner og netto
        _kjopTilFaktura.Clear();
        int rowIx = 0;
        if (_kjoptVarelinjer != null)
        {
            foreach (varelinje_grunnlag linje in _kjoptVarelinjer)
            {
                varelinje_grunnlag l = new varelinje_grunnlag();
                l = linje;
                if (true == (bool)kjoptVarelinjerDGV.Rows[rowIx].Cells["fakturer_denne"].EditedFormattedValue)
                {
                    l.provisjon = beregn_provisjoner(l, medlem_nr);
                    kjopt_faktura_rader.Add(l);
                    //netto_salg += l.netto_salg;
                    netto_kjop += l.netto;
                    //provisjon_salg += l.provisjon_salg;
                    provisjon_kjop += l.provisjon;
                    _kjopTilFaktura.Add(l);
                }
                ++rowIx;
            }
        }
        _salgTilFaktura.Clear();
        rowIx = 0;

        if (_solgtVarelinjer != null)
        {
            foreach (varelinje_grunnlag linje in _solgtVarelinjer)
            {
                varelinje_grunnlag l = new varelinje_grunnlag();
                l = linje;
                DataGridViewElementStates iii = solgtVarelinjerDGV.Rows[rowIx].Cells["fakturer_denne"].State;
                string sss = solgtVarelinjerDGV.Rows[rowIx].Cells["fakturer_denne"].Value.ToString();
                if (true == (bool)solgtVarelinjerDGV.Rows[rowIx].Cells["fakturer_denne"].EditedFormattedValue)
                {
                    //beregn_provisjoner(ref l, medlem_nr);
                    solgt_faktura_rader.Add(l);
                    netto_salg += l.netto;
                    //netto_kjop += l.netto_kjop;
                    provisjon_salg += l.provisjon;
                    //provisjon_kjop += l.provisjon_kjop;
                    _salgTilFaktura.Add(l);
                }
                ++rowIx;
            }
        }
        brutto_salg = netto_salg - provisjon_salg;
        brutto_kjop = netto_kjop + provisjon_kjop;
        netto = brutto_salg - brutto_kjop;

        nettoSalgTB.Text = netto_salg.ToString();
        nettoKjopTB.Text = netto_kjop.ToString();
        provisjonSalgTB.Text = provisjon_salg.ToString();
        provisjonKjopTB.Text = provisjon_kjop.ToString();
        bruttoSalgTB.Text = brutto_salg.ToString();
        bruttoKjopTB.Text = (0 - brutto_kjop).ToString();
        sumTB.Text = netto.ToString();

        if (medlem_id > 0)
        {
            _nyFakturaGrunnlag.medlem_id = medlem_id;
            _nyFakturaGrunnlag.sum = netto;
            btn_produser_faktura.Enabled = true;
        }
        else MessageBox.Show("Ugylding medlemsnummer");
    }

    private void btn_beregn_oppgjor_Click(object sender, EventArgs e)
    {
        beregn_faktura_grunnlag_cb(sender, e);
    }

    //private void lagre_faktura()
    //{
    //    string faktura_folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
    //    string faktura_medl_id_ = "1-123";
    //    string file_name = string.Concat(faktura_medl_id_, ".pdf");
    //    faktura_folder = System.IO.Path.Combine(faktura_folder, "faktura");
    //    faktura_folder = System.IO.Path.Combine(faktura_folder, file_name);
    //}


    private void btn_produser_faktura_Click(object sender, EventArgs e)
    {
   //         lagre_faktura();
            try         //Legg til varelinjer i faktura tabell
            {
                faktura nyFaktura = new faktura();
                nyFaktura.sum = (decimal)_nyFakturaGrunnlag.sum;
                nyFaktura.medlem_id = _nyFakturaGrunnlag.medlem_id;
                nyFaktura.faktura_dato = DateTime.Now.Date;
                nyFaktura.auksjon_id = auksjonId;
                db.fakturas.InsertOnSubmit(nyFaktura);
                db.SubmitChanges();

                if (_kjopTilFaktura.Count > 0)
                {
                    foreach (varelinje_grunnlag l in _kjopTilFaktura)
                    {
                        varelinje nyLinje = new varelinje();
                        nyLinje.ID = nyFaktura.ID;
                        nyLinje.utrop_nr = l.utrop_nr;
                        nyLinje.beskrivelse = l.navn;
                        nyLinje.gjenstand_id = l.gjenstand_id;
                        nyLinje.kjoper_id = l.kjoper_id;
                        nyLinje.selger_id = l.selger_id;
                        nyLinje.netto_kjop = (decimal)l.netto;
                        nyLinje.provisjon_kjop = (decimal)l.provisjon;
                        db.varelinjes.InsertOnSubmit(nyLinje);
                    }
                    db.SubmitChanges();
                }

                if (_salgTilFaktura.Count > 0)
                {
                    foreach (varelinje_grunnlag l in _salgTilFaktura)
                    {
                        varelinje nyLinje = new varelinje();
                        nyLinje.ID= nyFaktura.ID;
                        nyLinje.utrop_nr = l.utrop_nr;
                        nyLinje.gjenstand_id = l.gjenstand_id;
                        nyLinje.beskrivelse = l.navn;
                        nyLinje.kjoper_id = l.kjoper_id;
                        nyLinje.selger_id = l.selger_id;
                        nyLinje.netto_salg = (decimal)l.netto;
                        nyLinje.provisjon_salg = (decimal)l.provisjon;
                        db.varelinjes.InsertOnSubmit(nyLinje);
                    }
                    db.SubmitChanges();
                }
                
                //Sett fakturert_kjoper og fakturert selger
                foreach(varelinje_grunnlag kjVgr in _kjoptVarelinjer)
                {
                    if (kjVgr.fakturer_denne)
                    {
                        gjenstand kjOppdat = db.gjenstands.Single(p => p.ID== kjVgr.gjenstand_id);
                        kjOppdat.fakturert_kjoper = true;
                        db.SubmitChanges();
                    }
                }

                foreach (varelinje_grunnlag solgtVgr in _solgtVarelinjer)
                {
                    if (solgtVgr.fakturer_denne)
                    {
                        gjenstand solgtOppdat = db.gjenstands.Single(p => p.ID == solgtVgr.gjenstand_id);
                        solgtOppdat.fakturert_selger = true;
                        db.SubmitChanges();
                    }
                }
                
                //string msg = "Faktura " + ny_faktura.ID + " ble opprettet";
                //MessageBox.Show(msg);
               FakturaVisning fakt = new FakturaVisning(nyFaktura.ID);
                
                fakt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
 
    }
}
