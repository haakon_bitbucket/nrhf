﻿namespace AuksjonKjoperFaktura
{
    partial class FakturaVisning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.FakturaLinjerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fakturaViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.FakturaLinjerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // FakturaLinjerBindingSource
            // 
            this.FakturaLinjerBindingSource.DataMember = "varelinjer_";
            this.FakturaLinjerBindingSource.DataSource = typeof(AuksjonKjoperFaktura.FakturaLinjer);
            // 
            // fakturaViewer
            // 
            this.fakturaViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet2";
            reportDataSource1.Value = this.FakturaLinjerBindingSource;
            this.fakturaViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.fakturaViewer.LocalReport.ReportEmbeddedResource = "AuksjonKjoperFaktura.Faktura.rdlc";
            this.fakturaViewer.Location = new System.Drawing.Point(0, 0);
            this.fakturaViewer.Name = "fakturaViewer";
            this.fakturaViewer.Size = new System.Drawing.Size(800, 639);
            this.fakturaViewer.TabIndex = 0;
            // 
            // FakturaVisning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 639);
            this.Controls.Add(this.fakturaViewer);
            this.Name = "FakturaVisning";
            this.Text = "FakturaVisning";
            this.Load += new System.EventHandler(this.FakturaVisning_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FakturaLinjerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer fakturaViewer;
        private System.Windows.Forms.BindingSource FakturaLinjerBindingSource;
    
    }
}