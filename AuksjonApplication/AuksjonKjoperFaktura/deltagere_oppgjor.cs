﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonKjoperFaktura
{
    public partial class deltagere_oppgjor : Form
    {
        AuksjonDataClassesDataContext context;
        BindingSource bs_deltager;

        //TODO: id
        int denneAuksjonId;

        public deltagere_oppgjor()
        {
            InitializeComponent();
            context = new AuksjonDataClassesDataContext();

            setup_bindings();
            setup_kjoper_dgv();
        }

        public void setup_kjoper_dgv()
        {
            dgvDeltager.Columns["Gate1"].Visible = false;
            dgvDeltager.Columns["Gate2"].Visible = false;
            dgvDeltager.Columns["Postnummer"].Visible = false;
            dgvDeltager.Columns["Poststed"].Visible = false;
            dgvDeltager.Columns["Land"].Visible = false;
            dgvDeltager.RowHeadersVisible = false;
            dgvDeltager.ReadOnly = true;
            dgvDeltager.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvDeltager.Columns[0].FillWeight = 50;
            dgvDeltager.Columns[1].FillWeight = 60;
            dgvDeltager.Columns[2].FillWeight = 40;

            dgvStatusSolgt.RowHeadersVisible = false;
            dgvStatusSolgt.ReadOnly = true;
            dgvStatusSolgt.ScrollBars = ScrollBars.Vertical;
            dgvStatusSolgt.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvStatusSolgt.Columns[0].FillWeight = 30;
            dgvStatusSolgt.Columns[1].FillWeight = 30;

            dgvStatusKjopt.RowHeadersVisible = false;
            dgvStatusKjopt.ReadOnly = true;
            dgvStatusKjopt.ScrollBars = ScrollBars.Vertical;
            dgvStatusKjopt.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvStatusKjopt.Columns[0].FillWeight = 30;
            dgvStatusKjopt.Columns[1].FillWeight = 30;
        }

        public void setup_bindings()
        {
            bs_deltager = new BindingSource();
            bs_deltager.DataSource = context.medlems.Select( m => new Deltager(m.medlem_nummer, m.fornavn, m.mellomnavn, m.etternavn, m.gate1, m.gate2, m.postnummer, m.poststed, m.land ));
            bs_deltager.CurrentChanged += bsDeltager_CurrentChanged;

            denneAuksjonId = context.app_config_datas.Single().current_auksjon_id;

            dgvDeltager.DataSource = bs_deltager;
            tb_current_medlem.DataBindings.Add("Text", bs_deltager, "Medlemnummer");
            label14.DataBindings.Add("Text", bs_deltager, "Gate1");
            label15.DataBindings.Add("Text", bs_deltager, "Gate2");
            label12.DataBindings.Add("Text", bs_deltager, "Land");
            bsDeltager_CurrentChanged(this, new EventArgs());
        }

        private void bsDeltager_CurrentChanged(object sender, EventArgs e)
        {
            Deltager deltager = (Deltager)bs_deltager.Current;
            string nam = deltager.Fornavn ?? "";
            if (nam.Length > 0)
                nam += " ";
            if ((deltager.Mellomnavn ?? "").Length > 0)
                nam += (deltager.Mellomnavn + " ");
            if ((deltager.Etternavn ?? "").Length > 0)
                nam += deltager.Etternavn;
            label11.Text = nam;

            string adr = (deltager.Postnummer != null) ? deltager.Postnummer : "";
            if (adr.Length > 0)
                adr += " ";
            adr += deltager.Poststed;
            label16.Text = adr;

            var solgte_objekter = context.gjenstands.
                Where(g => g.auksjon_id == denneAuksjonId && g.selger_nummer == deltager.Medlemnummer).
                OrderByDescending(g => g.utrop_nummer).
                Select(g => new { GjenstandNr = g.utrop_nummer, Utropt = g.utropt ?? false, Navn = g.navn });
            BindingSource bsSolgt = new BindingSource();
            bsSolgt.DataSource = solgte_objekter;
            dgvStatusSolgt.DataSource = bsSolgt;

            var kjopte_objekter = context.gjenstands.
                Where(g => g.auksjon_id == denneAuksjonId && g.kjoper_nummer == deltager.Medlemnummer).
                OrderByDescending(g => g.utrop_nummer).
                Select(g => new { GjenstandNr = g.utrop_nummer, Oppgjort = g.fakturert_kjoper ?? false, Navn = g.navn });
            BindingSource bsKjopt = new BindingSource();
            bsKjopt.DataSource = kjopte_objekter;
            dgvStatusKjopt.DataSource = bsKjopt;
        }

        private void medlemBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            tb_current_medlem.Text = ((auksjonDataAksessLib.medlem_for_faktura_view)(((BindingSource)sender).Current)).medlem_nummer.ToString();
        }
      
        private void btn_forrige_medlem_Click(object sender, EventArgs e)
        {
            bs_deltager.MovePrevious();
        }

        private void btn_neste_medlem_Click(object sender, EventArgs e)
        {
            bs_deltager.MoveNext();
        }

        private void btn_siste_medlem_Click(object sender, EventArgs e)
        {
            bs_deltager.MoveLast();
        }

        private void btn_forste_medlem_Click(object sender, EventArgs e)
        {
            bs_deltager.MoveFirst();
        }

        private void tb_current_medlem_Leave(object sender, EventArgs e)
        {
            //int  in_nummer = Int16.Parse(((TextBox)sender).Text);
            //int ny_id = db.medlems.Single(m => m.medlem_nummer == in_nummer).medlem_id;
            //medlemBindingSource.Position = ny_id-1;
        }

        //Change current when current_medlem nr is entered.
        private void tb_current_medlem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                int in_nummer;
                try
                {
                    in_nummer = Int16.Parse(((TextBox)sender).Text);
                    var ny_obj = context.medlems.Single(m => m.medlem_nummer == in_nummer);
                    int index = context.medlems.ToList().IndexOf(ny_obj);
                    bs_deltager.Position = index;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                e.Handled = true;
                base.OnKeyPress(e);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Oppgjor oppgj = new Oppgjor(ref bs_deltager);
            oppgj.Show();
        }
    }
}
