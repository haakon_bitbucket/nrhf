﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Windows.Forms;
using auksjonDataAksessLib;
using Microsoft.Reporting.WinForms;


namespace AuksjonKjoperFaktura
{
    public partial class FakturaVisning : Form
    {
        AuksjonDataClassesDataContext db_;
        
        int faktura_id_ = 0;
        string faktura_medl_id_;
        FakturaLinjer fakturalinjer = new FakturaLinjer();

        public FakturaVisning(int faktura_id)
        {
            InitializeComponent();
            faktura_id_ = faktura_id;
            db_ = new AuksjonDataClassesDataContext();
        }

        private void FakturaVisning_Load(object sender, EventArgs e)
        {
            faktura fakt = db_.fakturas.Single(p => p.ID == faktura_id_);
            medlem medl = db_.medlems.Single(p => p.ID == fakt.medlem_id);
            
            List<ReportParameter> paramList = new List<ReportParameter>();

            string dato = fakt.faktura_dato.Value.Day.ToString() + "-" + fakt.faktura_dato.Value.Month.ToString() + "-" + fakt.faktura_dato.Value.Year.ToString();

            //Fornavn + ' ' + etternavn
            string medl_navn = medl.fornavn;
            if (medl_navn.Length > 0)
                medl_navn += " ";
            medl_navn += medl.etternavn;

            string medl_adresse = medl.gate1;// +", " + medl.gate2 + ", " + medl.postnummer + " " + medl.poststed + ", " + medl.land;
            paramList.Add(new ReportParameter("kjoper_adresse", medl.gate1 ?? "", true));   //Må sende alle parametre ellers feiler rapport renderen
            paramList.Add(new ReportParameter("kjoper_adresse2", medl.gate2 ?? "", true));
            paramList.Add(new ReportParameter("kjoper_postnummer", medl.postnummer ?? "", true));
            paramList.Add(new ReportParameter("kjoper_poststed", medl.poststed ?? "", true));
            paramList.Add(new ReportParameter("kjoper_land", medl.land ?? "", true));
            paramList.Add(new ReportParameter("medlem_nummer", medl.medlem_nummer.ToString(), true));
            paramList.Add(new ReportParameter("kjoper_navn", medl_navn, false));
            paramList.Add(new ReportParameter("sum_totalt", fakt.sum.ToString(), true));
            paramList.Add(new ReportParameter("faktura_dato", dato, true));
            faktura_medl_id_ = medl.medlem_nummer.ToString() + "-" + faktura_id_.ToString();
            paramList.Add(new ReportParameter("faktura_id", faktura_medl_id_, true));

            fakturaViewer.PrinterSettings.Copies = 2;

            try
            {
                //Transfer parameters
                this.fakturaViewer.LocalReport.SetParameters(paramList);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Exception inner = ex.InnerException;
                while (inner != null)
                {
                    msg = msg + "\r";
                    msg = msg + inner.Message;
                    inner = inner.InnerException;
                }
                MessageBox.Show(msg);
            }

            fakturalinjer.varelinjer_ = db_.varelinjes.Where(v => v.faktura_id == faktura_id_).Select(vl => vl).ToList<varelinje>();
            this.FakturaLinjerBindingSource.DataSource = fakturalinjer.varelinjer_;

            string faktura_folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);

            string file_name = string.Concat(faktura_medl_id_, ".pdf");
            faktura_folder = System.IO.Path.Combine(faktura_folder, "faktura");
            if (false == System.IO.Directory.Exists(faktura_folder))
                System.IO.Directory.CreateDirectory(faktura_folder);
            faktura_folder = System.IO.Path.Combine(faktura_folder, file_name);

            try
            {
                System.IO.File.Delete(faktura_folder);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            byte[] bytes = fakturaViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);

            System.IO.FileStream fs = new System.IO.FileStream(faktura_folder, System.IO.FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
            this.fakturaViewer.RefreshReport();
        }
    }
}
