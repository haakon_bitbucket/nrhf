﻿namespace AuksjonKjoperFaktura
{
    partial class deltagere_oppgjor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvStatusSolgt = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDeltager = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_forste_medlem = new System.Windows.Forms.Button();
            this.btn_forrige_medlem = new System.Windows.Forms.Button();
            this.tb_current_medlem = new System.Windows.Forms.TextBox();
            this.btn_neste_medlem = new System.Windows.Forms.Button();
            this.btn_siste_medlem = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvStatusKjopt = new System.Windows.Forms.DataGridView();
            this.medlemidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kontonummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxgjenstanderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.utropprioritetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gjenstandDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fakturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatusSolgt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltager)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatusKjopt)).BeginInit();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Medlem ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label11, 2);
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label12, 2);
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 17);
            this.label12.TabIndex = 39;
            this.label12.Text = "label12";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label16, 2);
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 78);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 17);
            this.label16.TabIndex = 37;
            this.label16.Text = "label16";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.dgvStatusSolgt, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.dgvDeltager, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvStatusKjopt, 2, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 322F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(997, 629);
            this.tableLayoutPanel2.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(501, 280);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 54;
            this.label2.Text = "Kjøpt ";
            // 
            // dgvStatusSolgt
            // 
            this.dgvStatusSolgt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.dgvStatusSolgt, 2);
            this.dgvStatusSolgt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStatusSolgt.Location = new System.Drawing.Point(3, 310);
            this.dgvStatusSolgt.Name = "dgvStatusSolgt";
            this.dgvStatusSolgt.Size = new System.Drawing.Size(492, 316);
            this.dgvStatusSolgt.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 280);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 53;
            this.label1.Text = "Solgt ";
            // 
            // dgvDeltager
            // 
            this.dgvDeltager.AllowUserToAddRows = false;
            this.dgvDeltager.AllowUserToDeleteRows = false;
            this.dgvDeltager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.dgvDeltager, 2);
            this.dgvDeltager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDeltager.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDeltager.Location = new System.Drawing.Point(3, 35);
            this.dgvDeltager.MultiSelect = false;
            this.dgvDeltager.Name = "dgvDeltager";
            this.dgvDeltager.ReadOnly = true;
            this.tableLayoutPanel2.SetRowSpan(this.dgvDeltager, 2);
            this.dgvDeltager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDeltager.Size = new System.Drawing.Size(492, 236);
            this.dgvDeltager.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.95943F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.04057F));
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(501, 35);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel2.SetRowSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(493, 236);
            this.tableLayoutPanel1.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 17);
            this.label14.TabIndex = 35;
            this.label14.Text = "label14";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(199, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 17);
            this.label15.TabIndex = 36;
            this.label15.Text = "label15";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_forste_medlem);
            this.flowLayoutPanel1.Controls.Add(this.btn_forrige_medlem);
            this.flowLayoutPanel1.Controls.Add(this.tb_current_medlem);
            this.flowLayoutPanel1.Controls.Add(this.btn_neste_medlem);
            this.flowLayoutPanel1.Controls.Add(this.btn_siste_medlem);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 166);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(190, 26);
            this.flowLayoutPanel1.TabIndex = 49;
            // 
            // btn_forste_medlem
            // 
            this.btn_forste_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_forste_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_forste_medlem.Location = new System.Drawing.Point(3, 3);
            this.btn_forste_medlem.Name = "btn_forste_medlem";
            this.btn_forste_medlem.Size = new System.Drawing.Size(25, 23);
            this.btn_forste_medlem.TabIndex = 48;
            this.btn_forste_medlem.Text = "|<";
            this.btn_forste_medlem.UseVisualStyleBackColor = true;
            this.btn_forste_medlem.Click += new System.EventHandler(this.btn_forste_medlem_Click);
            // 
            // btn_forrige_medlem
            // 
            this.btn_forrige_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_forrige_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_forrige_medlem.Location = new System.Drawing.Point(34, 3);
            this.btn_forrige_medlem.Name = "btn_forrige_medlem";
            this.btn_forrige_medlem.Size = new System.Drawing.Size(23, 23);
            this.btn_forrige_medlem.TabIndex = 44;
            this.btn_forrige_medlem.Text = "<";
            this.btn_forrige_medlem.UseVisualStyleBackColor = true;
            this.btn_forrige_medlem.Click += new System.EventHandler(this.btn_forrige_medlem_Click);
            // 
            // tb_current_medlem
            // 
            this.tb_current_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_current_medlem.Location = new System.Drawing.Point(63, 4);
            this.tb_current_medlem.Name = "tb_current_medlem";
            this.tb_current_medlem.Size = new System.Drawing.Size(55, 20);
            this.tb_current_medlem.TabIndex = 50;
            this.tb_current_medlem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_current_medlem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_current_medlem_KeyPress);
            // 
            // btn_neste_medlem
            // 
            this.btn_neste_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_neste_medlem.AutoSize = true;
            this.btn_neste_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_neste_medlem.Location = new System.Drawing.Point(124, 3);
            this.btn_neste_medlem.Name = "btn_neste_medlem";
            this.btn_neste_medlem.Size = new System.Drawing.Size(25, 23);
            this.btn_neste_medlem.TabIndex = 46;
            this.btn_neste_medlem.Text = ">";
            this.btn_neste_medlem.UseVisualStyleBackColor = true;
            this.btn_neste_medlem.Click += new System.EventHandler(this.btn_neste_medlem_Click);
            // 
            // btn_siste_medlem
            // 
            this.btn_siste_medlem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_siste_medlem.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_siste_medlem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_siste_medlem.Location = new System.Drawing.Point(155, 3);
            this.btn_siste_medlem.Name = "btn_siste_medlem";
            this.btn_siste_medlem.Size = new System.Drawing.Size(25, 23);
            this.btn_siste_medlem.TabIndex = 47;
            this.btn_siste_medlem.Text = ">|";
            this.btn_siste_medlem.UseVisualStyleBackColor = true;
            this.btn_siste_medlem.Click += new System.EventHandler(this.btn_siste_medlem_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Highlight;
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(199, 166);
            this.button1.Name = "button1";
            this.tableLayoutPanel1.SetRowSpan(this.button1, 2);
            this.button1.Size = new System.Drawing.Size(291, 67);
            this.button1.TabIndex = 50;
            this.button1.Text = "Til Oppgjør";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvStatusKjopt
            // 
            this.dgvStatusKjopt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.dgvStatusKjopt, 2);
            this.dgvStatusKjopt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStatusKjopt.Location = new System.Drawing.Point(501, 310);
            this.dgvStatusKjopt.Name = "dgvStatusKjopt";
            this.dgvStatusKjopt.Size = new System.Drawing.Size(493, 316);
            this.dgvStatusKjopt.TabIndex = 52;
            // 
            // medlemidDataGridViewTextBoxColumn
            // 
            this.medlemidDataGridViewTextBoxColumn.DataPropertyName = "medlem_id";
            this.medlemidDataGridViewTextBoxColumn.HeaderText = "medlem_id";
            this.medlemidDataGridViewTextBoxColumn.Name = "medlemidDataGridViewTextBoxColumn";
            // 
            // kontonummerDataGridViewTextBoxColumn
            // 
            this.kontonummerDataGridViewTextBoxColumn.DataPropertyName = "kontonummer";
            this.kontonummerDataGridViewTextBoxColumn.HeaderText = "kontonummer";
            this.kontonummerDataGridViewTextBoxColumn.Name = "kontonummerDataGridViewTextBoxColumn";
            // 
            // epostDataGridViewTextBoxColumn
            // 
            this.epostDataGridViewTextBoxColumn.DataPropertyName = "epost";
            this.epostDataGridViewTextBoxColumn.HeaderText = "epost";
            this.epostDataGridViewTextBoxColumn.Name = "epostDataGridViewTextBoxColumn";
            // 
            // maxgjenstanderDataGridViewTextBoxColumn
            // 
            this.maxgjenstanderDataGridViewTextBoxColumn.DataPropertyName = "max_gjenstander";
            this.maxgjenstanderDataGridViewTextBoxColumn.HeaderText = "max_gjenstander";
            this.maxgjenstanderDataGridViewTextBoxColumn.Name = "maxgjenstanderDataGridViewTextBoxColumn";
            // 
            // utropprioritetDataGridViewTextBoxColumn
            // 
            this.utropprioritetDataGridViewTextBoxColumn.DataPropertyName = "utrop_prioritet";
            this.utropprioritetDataGridViewTextBoxColumn.HeaderText = "utrop_prioritet";
            this.utropprioritetDataGridViewTextBoxColumn.Name = "utropprioritetDataGridViewTextBoxColumn";
            // 
            // gjenstandDataGridViewTextBoxColumn
            // 
            this.gjenstandDataGridViewTextBoxColumn.DataPropertyName = "gjenstand";
            this.gjenstandDataGridViewTextBoxColumn.HeaderText = "gjenstand";
            this.gjenstandDataGridViewTextBoxColumn.Name = "gjenstandDataGridViewTextBoxColumn";
            // 
            // fakturaDataGridViewTextBoxColumn
            // 
            this.fakturaDataGridViewTextBoxColumn.DataPropertyName = "faktura";
            this.fakturaDataGridViewTextBoxColumn.HeaderText = "faktura";
            this.fakturaDataGridViewTextBoxColumn.Name = "fakturaDataGridViewTextBoxColumn";
            // 
            // deltagere_oppgjor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 629);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "deltagere_oppgjor";
            this.Text = "deltagers_oppgjor";
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatusSolgt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltager)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatusKjopt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView dgvDeltager;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btn_forste_medlem;
        private System.Windows.Forms.Button btn_forrige_medlem;
        private System.Windows.Forms.TextBox tb_current_medlem;
        private System.Windows.Forms.Button btn_neste_medlem;
        private System.Windows.Forms.Button btn_siste_medlem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn medlemidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontonummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn epostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxgjenstanderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn utropprioritetDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gjenstandDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fakturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dgvStatusSolgt;
        private System.Windows.Forms.DataGridView dgvStatusKjopt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

    }
}