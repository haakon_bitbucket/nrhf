﻿namespace AuksjonKjoperFaktura
{
    partial class Oppgjor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label10 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.sumTB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nettoKjopTB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.provisjonKjopTB = new System.Windows.Forms.TextBox();
            this.bruttoKjopTB = new System.Windows.Forms.TextBox();
            this.nettoSalgTB = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.kjoptVarelinjerDGV = new System.Windows.Forms.DataGridView();
            this.solgtVarelinjerDGV = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.bruttoSalgTB = new System.Windows.Forms.TextBox();
            this.medlemidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kontonummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxgjenstanderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oppgjorTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.provisjonSalgTB = new System.Windows.Forms.TextBox();
            this.btn_beregn_oppgjor = new System.Windows.Forms.Button();
            this.btn_produser_faktura = new System.Windows.Forms.Button();
            this.utropprioritetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gjenstandDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fakturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.kjoptVarelinjerDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.solgtVarelinjerDGV)).BeginInit();
            this.oppgjorTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(179, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 17);
            this.label10.TabIndex = 40;
            this.label10.Text = "label10";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Medlem";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 151);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 17);
            this.label12.TabIndex = 39;
            this.label12.Text = "label12";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label13, 2);
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 17);
            this.label13.TabIndex = 34;
            this.label13.Text = "label13";
            // 
            // sumTB
            // 
            this.sumTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sumTB.Location = new System.Drawing.Point(212, 153);
            this.sumTB.Name = "sumTB";
            this.sumTB.Size = new System.Drawing.Size(124, 20);
            this.sumTB.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Sum";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.oppgjorTableLayoutPanel.SetColumnSpan(this.label5, 2);
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Oppgjør";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Kjøp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Netto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(110, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Provisjoner";
            // 
            // nettoKjopTB
            // 
            this.nettoKjopTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nettoKjopTB.Location = new System.Drawing.Point(59, 123);
            this.nettoKjopTB.Name = "nettoKjopTB";
            this.nettoKjopTB.Size = new System.Drawing.Size(45, 20);
            this.nettoKjopTB.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(212, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Brutto";
            // 
            // provisjonKjopTB
            // 
            this.provisjonKjopTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.provisjonKjopTB.Location = new System.Drawing.Point(110, 123);
            this.provisjonKjopTB.Name = "provisjonKjopTB";
            this.provisjonKjopTB.Size = new System.Drawing.Size(96, 20);
            this.provisjonKjopTB.TabIndex = 12;
            // 
            // bruttoKjopTB
            // 
            this.bruttoKjopTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bruttoKjopTB.Location = new System.Drawing.Point(212, 123);
            this.bruttoKjopTB.Name = "bruttoKjopTB";
            this.bruttoKjopTB.Size = new System.Drawing.Size(124, 20);
            this.bruttoKjopTB.TabIndex = 16;
            // 
            // nettoSalgTB
            // 
            this.nettoSalgTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nettoSalgTB.Location = new System.Drawing.Point(59, 93);
            this.nettoSalgTB.Name = "nettoSalgTB";
            this.nettoSalgTB.Size = new System.Drawing.Size(45, 20);
            this.nettoSalgTB.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 17);
            this.label16.TabIndex = 37;
            this.label16.Text = "label16";
            // 
            // kjoptVarelinjerDGV
            // 
            this.kjoptVarelinjerDGV.AllowUserToAddRows = false;
            this.kjoptVarelinjerDGV.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.kjoptVarelinjerDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.kjoptVarelinjerDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.kjoptVarelinjerDGV, 3);
            this.kjoptVarelinjerDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kjoptVarelinjerDGV.Location = new System.Drawing.Point(3, 254);
            this.kjoptVarelinjerDGV.Name = "kjoptVarelinjerDGV";
            this.kjoptVarelinjerDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kjoptVarelinjerDGV.Size = new System.Drawing.Size(1060, 260);
            this.kjoptVarelinjerDGV.TabIndex = 1;
            // 
            // solgtVarelinjerDGV
            // 
            this.solgtVarelinjerDGV.AllowUserToAddRows = false;
            this.solgtVarelinjerDGV.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.solgtVarelinjerDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.solgtVarelinjerDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.solgtVarelinjerDGV, 3);
            this.solgtVarelinjerDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.solgtVarelinjerDGV.Location = new System.Drawing.Point(3, 559);
            this.solgtVarelinjerDGV.Name = "solgtVarelinjerDGV";
            this.solgtVarelinjerDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.solgtVarelinjerDGV.Size = new System.Drawing.Size(1060, 282);
            this.solgtVarelinjerDGV.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(179, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 17);
            this.label15.TabIndex = 36;
            this.label15.Text = "label15";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(179, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 17);
            this.label17.TabIndex = 38;
            this.label17.Text = "label17";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 17);
            this.label14.TabIndex = 35;
            this.label14.Text = "label14";
            // 
            // bruttoSalgTB
            // 
            this.bruttoSalgTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bruttoSalgTB.Location = new System.Drawing.Point(212, 93);
            this.bruttoSalgTB.Name = "bruttoSalgTB";
            this.bruttoSalgTB.Size = new System.Drawing.Size(124, 20);
            this.bruttoSalgTB.TabIndex = 14;
            // 
            // medlemidDataGridViewTextBoxColumn
            // 
            this.medlemidDataGridViewTextBoxColumn.DataPropertyName = "medlem_id";
            this.medlemidDataGridViewTextBoxColumn.HeaderText = "medlem_id";
            this.medlemidDataGridViewTextBoxColumn.Name = "medlemidDataGridViewTextBoxColumn";
            // 
            // kontonummerDataGridViewTextBoxColumn
            // 
            this.kontonummerDataGridViewTextBoxColumn.DataPropertyName = "kontonummer";
            this.kontonummerDataGridViewTextBoxColumn.HeaderText = "kontonummer";
            this.kontonummerDataGridViewTextBoxColumn.Name = "kontonummerDataGridViewTextBoxColumn";
            // 
            // epostDataGridViewTextBoxColumn
            // 
            this.epostDataGridViewTextBoxColumn.DataPropertyName = "epost";
            this.epostDataGridViewTextBoxColumn.HeaderText = "epost";
            this.epostDataGridViewTextBoxColumn.Name = "epostDataGridViewTextBoxColumn";
            // 
            // maxgjenstanderDataGridViewTextBoxColumn
            // 
            this.maxgjenstanderDataGridViewTextBoxColumn.DataPropertyName = "max_gjenstander";
            this.maxgjenstanderDataGridViewTextBoxColumn.HeaderText = "max_gjenstander";
            this.maxgjenstanderDataGridViewTextBoxColumn.Name = "maxgjenstanderDataGridViewTextBoxColumn";
            // 
            // oppgjorTableLayoutPanel
            // 
            this.oppgjorTableLayoutPanel.BackColor = System.Drawing.SystemColors.Control;
            this.oppgjorTableLayoutPanel.ColumnCount = 4;
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52F));
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48F));
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.oppgjorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.oppgjorTableLayoutPanel.Controls.Add(this.label5, 0, 0);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label7, 0, 5);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label2, 0, 4);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label4, 0, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.sumTB, 3, 5);
            this.oppgjorTableLayoutPanel.Controls.Add(this.bruttoKjopTB, 3, 4);
            this.oppgjorTableLayoutPanel.Controls.Add(this.bruttoSalgTB, 3, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label6, 3, 2);
            this.oppgjorTableLayoutPanel.Controls.Add(this.provisjonKjopTB, 2, 4);
            this.oppgjorTableLayoutPanel.Controls.Add(this.provisjonSalgTB, 2, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label3, 2, 2);
            this.oppgjorTableLayoutPanel.Controls.Add(this.nettoKjopTB, 1, 4);
            this.oppgjorTableLayoutPanel.Controls.Add(this.nettoSalgTB, 1, 3);
            this.oppgjorTableLayoutPanel.Controls.Add(this.label1, 1, 2);
            this.oppgjorTableLayoutPanel.Location = new System.Drawing.Point(406, 33);
            this.oppgjorTableLayoutPanel.Name = "oppgjorTableLayoutPanel";
            this.oppgjorTableLayoutPanel.RowCount = 6;
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.45238F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.88095F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.oppgjorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.oppgjorTableLayoutPanel.Size = new System.Drawing.Size(339, 184);
            this.oppgjorTableLayoutPanel.TabIndex = 42;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Salg";
            // 
            // provisjonSalgTB
            // 
            this.provisjonSalgTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.provisjonSalgTB.Location = new System.Drawing.Point(110, 93);
            this.provisjonSalgTB.Name = "provisjonSalgTB";
            this.provisjonSalgTB.Size = new System.Drawing.Size(96, 20);
            this.provisjonSalgTB.TabIndex = 10;
            // 
            // btn_beregn_oppgjor
            // 
            this.btn_beregn_oppgjor.BackColor = System.Drawing.SystemColors.GrayText;
            this.btn_beregn_oppgjor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_beregn_oppgjor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_beregn_oppgjor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_beregn_oppgjor.Location = new System.Drawing.Point(33, 99);
            this.btn_beregn_oppgjor.Name = "btn_beregn_oppgjor";
            this.btn_beregn_oppgjor.Size = new System.Drawing.Size(267, 52);
            this.btn_beregn_oppgjor.TabIndex = 19;
            this.btn_beregn_oppgjor.Text = "Rekalkuler sum";
            this.btn_beregn_oppgjor.UseVisualStyleBackColor = false;
            this.btn_beregn_oppgjor.Click += new System.EventHandler(this.btn_beregn_oppgjor_Click);
            // 
            // btn_produser_faktura
            // 
            this.btn_produser_faktura.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn_produser_faktura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_produser_faktura.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_produser_faktura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_produser_faktura.Location = new System.Drawing.Point(33, 3);
            this.btn_produser_faktura.Name = "btn_produser_faktura";
            this.btn_produser_faktura.Size = new System.Drawing.Size(267, 90);
            this.btn_produser_faktura.TabIndex = 20;
            this.btn_produser_faktura.Text = "Lag faktura";
            this.btn_produser_faktura.UseVisualStyleBackColor = false;
            this.btn_produser_faktura.Click += new System.EventHandler(this.btn_produser_faktura_Click);
            // 
            // utropprioritetDataGridViewTextBoxColumn
            // 
            this.utropprioritetDataGridViewTextBoxColumn.DataPropertyName = "utrop_prioritet";
            this.utropprioritetDataGridViewTextBoxColumn.HeaderText = "utrop_prioritet";
            this.utropprioritetDataGridViewTextBoxColumn.Name = "utropprioritetDataGridViewTextBoxColumn";
            // 
            // gjenstandDataGridViewTextBoxColumn
            // 
            this.gjenstandDataGridViewTextBoxColumn.DataPropertyName = "gjenstand";
            this.gjenstandDataGridViewTextBoxColumn.HeaderText = "gjenstand";
            this.gjenstandDataGridViewTextBoxColumn.Name = "gjenstandDataGridViewTextBoxColumn";
            // 
            // fakturaDataGridViewTextBoxColumn
            // 
            this.fakturaDataGridViewTextBoxColumn.DataPropertyName = "faktura";
            this.fakturaDataGridViewTextBoxColumn.HeaderText = "faktura";
            this.fakturaDataGridViewTextBoxColumn.Name = "fakturaDataGridViewTextBoxColumn";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 20);
            this.label8.TabIndex = 19;
            this.label8.Text = "Kjøpte gjenstander";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.Controls.Add(this.label10, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label17, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 33);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57.37705F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.62295F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(374, 184);
            this.tableLayoutPanel1.TabIndex = 43;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 536);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(161, 20);
            this.label9.TabIndex = 20;
            this.label9.Text = "Solgte gjenstander";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.89571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.45779F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.73734F));
            this.tableLayoutPanel2.Controls.Add(this.solgtVarelinjerDGV, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.kjoptVarelinjerDGV, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.oppgjorTableLayoutPanel, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.65401F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.34599F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 266F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 288F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1066, 865);
            this.tableLayoutPanel2.TabIndex = 51;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel3.Controls.Add(this.btn_beregn_oppgjor, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_produser_faktura, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(751, 63);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62.98701F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.01299F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(303, 154);
            this.tableLayoutPanel3.TabIndex = 44;
            // 
            // Oppgjor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 865);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "Oppgjor";
            this.Text = "Oppgjør";
            ((System.ComponentModel.ISupportInitialize)(this.kjoptVarelinjerDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.solgtVarelinjerDGV)).EndInit();
            this.oppgjorTableLayoutPanel.ResumeLayout(false);
            this.oppgjorTableLayoutPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

//        private System.Windows.Forms.BindingSource medlemBindingSource;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox sumTB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nettoKjopTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox provisjonKjopTB;
        private System.Windows.Forms.TextBox bruttoKjopTB;
        private System.Windows.Forms.TextBox nettoSalgTB;
//        private System.Windows.Forms.BindingSource varelinjeBindingSource;
        private System.Windows.Forms.DataGridView kjoptVarelinjerDGV;
        private System.Windows.Forms.DataGridView solgtVarelinjerDGV;
        private System.Windows.Forms.TextBox bruttoSalgTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn medlemidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontonummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn epostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxgjenstanderDataGridViewTextBoxColumn;
        private System.Windows.Forms.TableLayoutPanel oppgjorTableLayoutPanel;
        private System.Windows.Forms.TextBox provisjonSalgTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_beregn_oppgjor;
        private System.Windows.Forms.Button btn_produser_faktura;
        private System.Windows.Forms.DataGridViewTextBoxColumn utropprioritetDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gjenstandDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fakturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;



    }
}