﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using auksjonDataAksessLib;

namespace AuksjonAuksjonarius
{
    public partial class AuksjonAuksjonarius : Form
    {
        AuksjonDataClassesDataContext db;
        auksjon denne_auksjon_;
        int denne_auksjon_id;
        BindingSource bsForhaandsbud = new BindingSource();

        public AuksjonAuksjonarius()
        {
            InitializeComponent();
            db = new AuksjonDataClassesDataContext();
            db.Log = Console.Out;
            denne_auksjon_id = db.app_config_datas.Single().current_auksjon_id;

            try
            {
                var medl_ids = from medl in db.medlems orderby medl.medlem_nummer select medl.medlem_nummer;
                if (medl_ids.Count() == 0)
                {
                    MessageBox.Show("No database connection\nOpen the database connection tool to connect");
                    Application.Exit();
                }

                var lookup_source = new AutoCompleteStringCollection();
                foreach (int id in medl_ids)
                    lookup_source.Add(id.ToString());
                kjoper_id_tb.AutoCompleteCustomSource = lookup_source;
                kjoper_id_tb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                kjoper_id_tb.AutoCompleteSource = AutoCompleteSource.CustomSource;
                denne_auksjon_ = db.auksjons.Single(p => p.ID == denne_auksjon_id);
                var medlem_ids = db.medlems.Select(p => p.medlem_nummer);

                gjenstandBindingSource.DataSource = db.GetTable<gjenstand>().Where(g => g.auksjon_id == denne_auksjon_id).OrderBy(g => g.utrop_nummer);
                GjenstanderDGV.DataSource = gjenstandBindingSource;
                bindingNavigator1.BindingSource = gjenstandBindingSource;
                GjenstanderDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                GjenstanderDGV.Columns["ID"].Visible = false;
                GjenstanderDGV.Columns["auksjon_id"].Visible = false;
                GjenstanderDGV.Columns["prioritet"].Visible = false;
                GjenstanderDGV.Columns["beskrivelse"].Visible = false;
                GjenstanderDGV.Columns["selger_id"].Visible = false;
                GjenstanderDGV.Columns["kjoper_id"].Visible = false;
                GjenstanderDGV.Columns["fakturert_selger"].Visible = false;
                GjenstanderDGV.Columns["fakturert_kjoper"].Visible = false;
                GjenstanderDGV.Columns["auksjon"].Visible = false;
                GjenstanderDGV.Columns["vurdering"].Visible = false;
                GjenstanderDGV.Columns["utrop_nummer"].HeaderText = "Nummer";
                GjenstanderDGV.Columns["navn"].HeaderText = "Navn";
                GjenstanderDGV.Columns["vurdering"].HeaderText = "Vurdering";
                GjenstanderDGV.Columns["minstepris"].HeaderText = "Minstepris";
                GjenstanderDGV.Columns["Pris"].HeaderText = "Pris";
                GjenstanderDGV.Columns["selger_nummer"].HeaderText = "Selger";
                GjenstanderDGV.Columns["kjoper_nummer"].HeaderText = "Kjøper";
                GjenstanderDGV.Columns["utropt"].HeaderText = "Utropt";
                
                dgvForhaandsbud.DataSource = bsForhaandsbud;
                dgvForhaandsbud.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgvForhaandsbud.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nPossible reason:No database connection\nOpen the database connection tool to connect to the database\nThen restart the program");
                Application.Exit();
            }
        }

        private void tilslag_btn_Click(object sender, EventArgs e)
        {
            try
            {
                int kjoper_nummer;
                if (!int.TryParse(kjoper_id_tb.Text, out kjoper_nummer) || kjoper_nummer < 1)
                {
                    MessageBox.Show("Ugyldig medlem nummer", "Tilslag");
                    return;
                }
                medlem kjoper = db.medlems.SingleOrDefault(p => p.medlem_nummer == kjoper_nummer);
                if (kjoper == default(medlem))
                {
                    MessageBox.Show("Ukjent medlemsnummer", "Tilslag");
                    return;
                }
                kjoper_nummer = kjoper.medlem_nummer;

                decimal pris = 0;
                decimal.TryParse(pris_tb.Text, out pris);

                if (minstepris_tb.Text.Length > 0)
                    if (!decimal.TryParse(pris_tb.Text, out pris) || pris == (decimal)0.0 || pris < decimal.Parse(minstepris_tb.Text))
                    {
                        DialogResult res = MessageBox.Show("Prisen er lavere enn minstepris\rBruke denne prisen?", "Tilslag", MessageBoxButtons.YesNo);
                        if (res == System.Windows.Forms.DialogResult.No)
                            return;
                    }

                int gjenstand_id = ((gjenstand)gjenstandBindingSource.Current).ID;
                gjenstand gj = db.gjenstands.Single(p => p.ID== gjenstand_id);
                gj.ID = gjenstand_id;
                gj.kjoper_id = kjoper.ID;
                gj.kjoper_nummer = kjoper_nummer;
                gj.pris = pris;
                gj.utropt = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            try
            {
                db.SubmitChanges();
                // GjenstanderDGV.Refresh();
                scrollGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Dispose();
                db = new AuksjonDataClassesDataContext();
            }
            SetFokusTilPris();
        }

        private void usolgt_btn_Click(object sender, EventArgs e)
        {
            int gjenstand_id = ((gjenstand)gjenstandBindingSource.Current).ID;
            gjenstand gj = db.gjenstands.Single(p => p.ID == gjenstand_id);
            gj.kjoper_id = 0;
            gj.kjoper_nummer = 0;
            gj.pris = (decimal)0;
            gj.utropt = true;
            try
            {
                db.SubmitChanges();
                GjenstanderDGV.Refresh();
                scrollGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Dispose();
                db = new AuksjonDataClassesDataContext();
            }
            SetFokusTilPris();
        }

        private void ikkeutropt_btn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Er du sikker på at du vil sette til 'Ikke utropt'?", "Ikke utropt", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                return;
            //int gjenstand_id = (int)GjenstanderDGV.CurrentRow.Cells["gjenstand_id"].Value;
            int gjenstand_id = ((gjenstand)gjenstandBindingSource.Current).ID;
            gjenstand gj = db.gjenstands.Single(p => p.ID == gjenstand_id);
            gj.kjoper_id = 0;
            gj.kjoper_nummer = 0;
            gj.pris = (decimal)0;
            gj.utropt = false;
            try
            {
                db.SubmitChanges();
                GjenstanderDGV.Refresh();
                //GjenstanderDGV.CurrentRow.Cells["pris"].Value = (decimal)0.0;
                //GjenstanderDGV.CurrentRow.Cells["utropt"].Value = false;
                //GjenstanderDGV.CurrentRow.Cells["kjoper_id_tb"].Value = 0;
                scrollGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Dispose();
                db = new AuksjonDataClassesDataContext();
            }
            SetFokusTilPris();
        }

        private void SetFokusTilPris()
        {
            TextBox tb = (TextBox)this.Controls.Find("pris_tb", true).FirstOrDefault();
            if (tb.CanSelect)
            {
                tb.Select();
            }
        }

        private void SetFokusTilKjoper()
        {
            TextBox tb = (TextBox)this.Controls.Find("kjoper_id_tb", true).FirstOrDefault();
            if (tb.CanSelect)
            {
                tb.Select();
            }
        }

        private void SetFokusTilTilslag()
        {
            Button btn = (Button)this.Controls.Find("tilslag_btn", true).FirstOrDefault();
            if (btn.CanSelect)
            {
                btn.Select();
            }
        }

        private void scrollGrid()
        {
            gjenstandBindingSource.MoveNext();
            return;

            int selectedIndex = GjenstanderDGV.CurrentRow.Index;
            if (selectedIndex < GjenstanderDGV.RowCount - 1)
                ++selectedIndex;
            // GjenstanderDGV.Rows[selectedIndex].Cells["utrop_nummer"].Selected = true;
            int halfWay = (GjenstanderDGV.DisplayedRowCount(false) / 2);
            if (GjenstanderDGV.FirstDisplayedScrollingRowIndex + halfWay > GjenstanderDGV.SelectedRows[0].Index ||
                (GjenstanderDGV.FirstDisplayedScrollingRowIndex + GjenstanderDGV.DisplayedRowCount(false) - halfWay) <= GjenstanderDGV.SelectedRows[0].Index)
            {
                int targetRow = GjenstanderDGV.SelectedRows[0].Index;

                targetRow = Math.Max(targetRow - halfWay, 0);
                GjenstanderDGV.FirstDisplayedScrollingRowIndex = targetRow;
            }
        }

        //Sjekk at kjoper_nummer finnes i medlemslisten (og i deltagere?)
        private void kjoper_id_tb_Leave(object sender, EventArgs e)
        {
        //    int kjoper_nummer;
        //    if (!int.TryParse(kjoper_id_tb.Text, out kjoper_nummer) || kjoper_nummer < 1)
        //    {
        //        MessageBox.Show("Ugyldig medlem nummer");
        //        return;
        //    }
        //    medlem medl;
        //    try
        //    {
        //        medl = db.medlems.Single(p => p.medlem_nummer == kjoper_nummer);
        //        fornavn_lbl.Text = medl.fornavn;
        //        etternavn_lbl.Text = medl.etternavn;
        //    }
        //    catch
        //    {
        //        MessageBox.Show("Finner ikke medlem " + kjoper_nummer.ToString(), "Ugyldig medlem nummer");
        //    }
        }

        private void gjenstandBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            //Utropt utropt = (utropt)gjenstandBindingSource.Current;
            gjenstand gj_aktiv = (gjenstand)gjenstandBindingSource.Current;
            bsForhaandsbud.DataSource = db.forhaandsbuds.Where(f => f.gjenstand_id == gj_aktiv.ID).OrderByDescending(f => f.pris).Select(f => new { BudgiverNr = f.medlem.medlem_nummer, Bud = f.pris });
            dgvForhaandsbud.Height = (dgvForhaandsbud.ColumnHeadersHeight + 1) * (1 + System.Math.Min(3, dgvForhaandsbud.Rows.Count));

            denne_auksjon_.last_sold_item = gj_aktiv.utrop_nummer ?? 1;
            db.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        private void kjoper_id_tb_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return)
            {
                e.Handled = true;
            }
        }

        private void kjoper_id_tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return)
            {
                e.Handled = true;
                SetFokusTilTilslag();
            }
        }

        private void pris_tb_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return)
            {
                e.Handled = true;
            }
        }

        private void pris_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                e.Handled = true;
                SetFokusTilKjoper();
            }
        }
    }

    public class Utrop
    {
        public int ID { get; set; }
        public int Nummer { get; set; }
        public string Navn { get; set; }
        public string Vurdering { get; set; }
        public decimal Minstepris { get; set; }
        public decimal Pris { get; set; }
        public int Selger { get; set; }
        public int Kjøper { get; set; }
        public bool Utropt { get; set; }
        public string Kommentar { get; set; }
    }
}
