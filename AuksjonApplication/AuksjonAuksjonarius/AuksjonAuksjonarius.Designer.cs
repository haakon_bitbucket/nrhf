﻿namespace AuksjonAuksjonarius
{
    partial class AuksjonAuksjonarius
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuksjonAuksjonarius));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.GjenstanderDGV = new System.Windows.Forms.DataGridView();
            this.pris = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kjoper_nummer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.kjoper_id_tb = new System.Windows.Forms.TextBox();
            this.gjenstandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tilslag_btn = new System.Windows.Forms.Button();
            this.pris_tb = new System.Windows.Forms.TextBox();
            this.etternavn_lbl = new System.Windows.Forms.Label();
            this.fornavn_lbl = new System.Windows.Forms.Label();
            this.usolgt_btn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNavn = new System.Windows.Forms.Label();
            this.dgvForhaandsbud = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.minstepris_tb = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ikkeutropt_btn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GjenstanderDGV)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gjenstandBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvForhaandsbud)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(949, 25);
            this.bindingNavigator1.TabIndex = 0;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // GjenstanderDGV
            // 
            this.GjenstanderDGV.AllowUserToAddRows = false;
            this.GjenstanderDGV.AllowUserToDeleteRows = false;
            this.GjenstanderDGV.AllowUserToOrderColumns = true;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GjenstanderDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GjenstanderDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.GjenstanderDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GjenstanderDGV.Dock = System.Windows.Forms.DockStyle.Top;
            this.GjenstanderDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GjenstanderDGV.Location = new System.Drawing.Point(0, 25);
            this.GjenstanderDGV.MultiSelect = false;
            this.GjenstanderDGV.Name = "GjenstanderDGV";
            this.GjenstanderDGV.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GjenstanderDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.GjenstanderDGV.RowTemplate.Height = 30;
            this.GjenstanderDGV.RowTemplate.ReadOnly = true;
            this.GjenstanderDGV.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GjenstanderDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GjenstanderDGV.Size = new System.Drawing.Size(949, 193);
            this.GjenstanderDGV.TabIndex = 1;
            this.GjenstanderDGV.TabStop = false;
            // 
            // pris
            // 
            this.pris.DataPropertyName = "pris";
            this.pris.HeaderText = "Pris";
            this.pris.Name = "pris";
            this.pris.ReadOnly = true;
            // 
            // kjoper_nummer
            // 
            this.kjoper_nummer.DataPropertyName = "kjoper_nummer";
            this.kjoper_nummer.HeaderText = "Kjoper Nr";
            this.kjoper_nummer.Name = "kjoper_nummer";
            this.kjoper_nummer.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.kjoper_id_tb);
            this.groupBox1.Controls.Add(this.tilslag_btn);
            this.groupBox1.Controls.Add(this.pris_tb);
            this.groupBox1.Controls.Add(this.etternavn_lbl);
            this.groupBox1.Controls.Add(this.fornavn_lbl);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 436);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(603, 157);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tilslag";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 17);
            this.label8.TabIndex = 49;
            this.label8.Text = "Pris";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(185, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 17);
            this.label6.TabIndex = 48;
            this.label6.Text = "Kjøper medlem nr.";
            // 
            // kjoper_id_tb
            // 
            this.kjoper_id_tb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gjenstandBindingSource, "kjoper_nummer", true));
            this.kjoper_id_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kjoper_id_tb.Location = new System.Drawing.Point(188, 52);
            this.kjoper_id_tb.Name = "kjoper_id_tb";
            this.kjoper_id_tb.Size = new System.Drawing.Size(154, 30);
            this.kjoper_id_tb.TabIndex = 2;
            this.kjoper_id_tb.Text = "0";
            this.kjoper_id_tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.kjoper_id_tb_KeyDown);
            this.kjoper_id_tb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.kjoper_id_tb_KeyUp);
            // 
            // gjenstandBindingSource
            // 
            this.gjenstandBindingSource.DataSource = typeof(auksjonDataAksessLib.gjenstand);
            this.gjenstandBindingSource.CurrentChanged += new System.EventHandler(this.gjenstandBindingSource_CurrentChanged);
            // 
            // tilslag_btn
            // 
            this.tilslag_btn.BackColor = System.Drawing.Color.LimeGreen;
            this.tilslag_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tilslag_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tilslag_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tilslag_btn.Location = new System.Drawing.Point(405, 52);
            this.tilslag_btn.Name = "tilslag_btn";
            this.tilslag_btn.Size = new System.Drawing.Size(154, 33);
            this.tilslag_btn.TabIndex = 3;
            this.tilslag_btn.Text = "&Tilslag";
            this.tilslag_btn.UseVisualStyleBackColor = false;
            this.tilslag_btn.Click += new System.EventHandler(this.tilslag_btn_Click);
            // 
            // pris_tb
            // 
            this.pris_tb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gjenstandBindingSource, "pris", true));
            this.pris_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pris_tb.Location = new System.Drawing.Point(11, 52);
            this.pris_tb.Name = "pris_tb";
            this.pris_tb.Size = new System.Drawing.Size(154, 30);
            this.pris_tb.TabIndex = 1;
            this.pris_tb.Text = "0";
            this.pris_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pris_tb_KeyPress);
            this.pris_tb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.pris_tb_KeyUp);
            // 
            // etternavn_lbl
            // 
            this.etternavn_lbl.AutoSize = true;
            this.etternavn_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etternavn_lbl.Location = new System.Drawing.Point(8, 121);
            this.etternavn_lbl.Name = "etternavn_lbl";
            this.etternavn_lbl.Size = new System.Drawing.Size(78, 17);
            this.etternavn_lbl.TabIndex = 39;
            this.etternavn_lbl.Text = "Etternavn";
            // 
            // fornavn_lbl
            // 
            this.fornavn_lbl.AutoSize = true;
            this.fornavn_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fornavn_lbl.Location = new System.Drawing.Point(8, 94);
            this.fornavn_lbl.Name = "fornavn_lbl";
            this.fornavn_lbl.Size = new System.Drawing.Size(67, 17);
            this.fornavn_lbl.TabIndex = 37;
            this.fornavn_lbl.Text = "Fornavn";
            // 
            // usolgt_btn
            // 
            this.usolgt_btn.BackColor = System.Drawing.Color.Red;
            this.usolgt_btn.FlatAppearance.BorderSize = 2;
            this.usolgt_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.usolgt_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.usolgt_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usolgt_btn.Location = new System.Drawing.Point(32, 52);
            this.usolgt_btn.Name = "usolgt_btn";
            this.usolgt_btn.Size = new System.Drawing.Size(154, 34);
            this.usolgt_btn.TabIndex = 44;
            this.usolgt_btn.TabStop = false;
            this.usolgt_btn.Text = "&Usolgt";
            this.usolgt_btn.UseVisualStyleBackColor = false;
            this.usolgt_btn.Click += new System.EventHandler(this.usolgt_btn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblNavn);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(427, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(499, 200);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 25);
            this.label7.TabIndex = 8;
            this.label7.Text = "Vurdering:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gjenstandBindingSource, "vurdering", true));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(132, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Vurdering";
            // 
            // lblNavn
            // 
            this.lblNavn.AutoSize = true;
            this.lblNavn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gjenstandBindingSource, "navn", true));
            this.lblNavn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNavn.Location = new System.Drawing.Point(6, 36);
            this.lblNavn.Name = "lblNavn";
            this.lblNavn.Size = new System.Drawing.Size(116, 25);
            this.lblNavn.TabIndex = 2;
            this.lblNavn.Text = "Gjenstand:";
            // 
            // dgvForhaandsbud
            // 
            this.dgvForhaandsbud.AllowUserToAddRows = false;
            this.dgvForhaandsbud.AllowUserToDeleteRows = false;
            this.dgvForhaandsbud.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Green;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvForhaandsbud.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvForhaandsbud.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvForhaandsbud.DefaultCellStyle = dataGridViewCellStyle26;
            this.dgvForhaandsbud.Location = new System.Drawing.Point(19, 50);
            this.dgvForhaandsbud.MultiSelect = false;
            this.dgvForhaandsbud.Name = "dgvForhaandsbud";
            this.dgvForhaandsbud.ReadOnly = true;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Green;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvForhaandsbud.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dgvForhaandsbud.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Green;
            this.dgvForhaandsbud.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.dgvForhaandsbud.RowTemplate.Height = 18;
            this.dgvForhaandsbud.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvForhaandsbud.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvForhaandsbud.Size = new System.Drawing.Size(349, 74);
            this.dgvForhaandsbud.TabIndex = 9;
            this.dgvForhaandsbud.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Minstepris:";
            // 
            // minstepris_tb
            // 
            this.minstepris_tb.AutoSize = true;
            this.minstepris_tb.BackColor = System.Drawing.SystemColors.Control;
            this.minstepris_tb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gjenstandBindingSource, "minstepris", true));
            this.minstepris_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minstepris_tb.ForeColor = System.Drawing.Color.Red;
            this.minstepris_tb.Location = new System.Drawing.Point(148, 142);
            this.minstepris_tb.Name = "minstepris_tb";
            this.minstepris_tb.Size = new System.Drawing.Size(111, 25);
            this.minstepris_tb.TabIndex = 5;
            this.minstepris_tb.Text = "Minstepris";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Forhåndsbud:";
            // 
            // ikkeutropt_btn
            // 
            this.ikkeutropt_btn.BackColor = System.Drawing.Color.Yellow;
            this.ikkeutropt_btn.FlatAppearance.BorderSize = 2;
            this.ikkeutropt_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ikkeutropt_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ikkeutropt_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ikkeutropt_btn.Location = new System.Drawing.Point(32, 106);
            this.ikkeutropt_btn.Name = "ikkeutropt_btn";
            this.ikkeutropt_btn.Size = new System.Drawing.Size(154, 34);
            this.ikkeutropt_btn.TabIndex = 50;
            this.ikkeutropt_btn.TabStop = false;
            this.ikkeutropt_btn.Text = "&Ikke utropt";
            this.ikkeutropt_btn.UseVisualStyleBackColor = false;
            this.ikkeutropt_btn.Click += new System.EventHandler(this.ikkeutropt_btn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ikkeutropt_btn);
            this.groupBox3.Controls.Add(this.usolgt_btn);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(687, 436);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(239, 157);
            this.groupBox3.TabIndex = 43;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kansellering";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.minstepris_tb);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.dgvForhaandsbud);
            this.groupBox4.Location = new System.Drawing.Point(22, 224);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(399, 200);
            this.groupBox4.TabIndex = 44;
            this.groupBox4.TabStop = false;
            // 
            // AuksjonAuksjonarius
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 609);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GjenstanderDGV);
            this.Controls.Add(this.bindingNavigator1);
            this.Name = "AuksjonAuksjonarius";
            this.Text = "Auksjonarius";
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GjenstanderDGV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gjenstandBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvForhaandsbud)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.DataGridView GjenstanderDGV;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label etternavn_lbl;
        private System.Windows.Forms.Label fornavn_lbl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNavn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button tilslag_btn;
        private System.Windows.Forms.Button usolgt_btn;
        private System.Windows.Forms.TextBox pris_tb;
        private System.Windows.Forms.TextBox kjoper_id_tb;
        private System.Windows.Forms.Label minstepris_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ikkeutropt_btn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgvForhaandsbud;
        private System.Windows.Forms.DataGridViewTextBoxColumn pris;
        private System.Windows.Forms.DataGridViewTextBoxColumn kjoper_nummer;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.BindingSource gjenstandBindingSource;
    }
}

