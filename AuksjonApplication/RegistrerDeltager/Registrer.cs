﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;
using auksjonDataAksessLib;

namespace RegistrerDeltager
{
    public partial class Registrer : Form
    {
        private AuksjonDataClassesDataContext context;
        private BindingSource bs;
        private int auksjonId;
        private bool erRegistrert;
        private bool medlemChanged;

        public Registrer()
        {
            context = new AuksjonDataClassesDataContext();
            bs = new BindingSource {DataSource = context.GetTable<medlem>()};
            auksjonId = context.app_config_datas.Single().current_auksjon_id;
            //Properties.Settings.Default.
            InitializeComponent();
            InitBindings();
        }

        private void InitBindings()
        {
            tbFornavn.DataBindings.Add("Text", bs, "fornavn");
            tbMellomnavn.DataBindings.Add("Text", bs, "mellomnavn");
            tbEtternavn.DataBindings.Add("Text", bs, "etternavn");
            tbMedlemNummer.DataBindings.Add("Text", bs, "medlem_nummer");
            tbGate1.DataBindings.Add("Text", bs, "gate1");
            tbGate2.DataBindings.Add("Text", bs, "gate2");
            tbPostnummer.DataBindings.Add("Text", bs, "postnummer");
            tbPoststed.DataBindings.Add("Text", bs, "poststed");
            tbLand.DataBindings.Add("Text", bs, "land");
            tbEpost.DataBindings.Add("Text", bs, "epost");
            tbMaxGjenstander.DataBindings.Add("Text", bs, "max_gjenstander");
            tbPrioritet.DataBindings.Add("Text", bs, "utrop_prioritet");
            tb_current.DataBindings.Add("Text", bs, "medlem_nummer");
            tbKonto.DataBindings.Add("Text", bs, "kontonummer");

            bs.CurrentChanged += BsOnCurrentChanged;
            bs.DataMemberChanged += bs_DataMemberChanged;
            bs.CurrentItemChanged += bs_CurrentItemChanged;
            bs.ListChanged += bs_ListChanged;
            
            medlemChanged = false;

            UpdateRegistrerCheck();
        }

        void bs_ListChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            int curr = ((medlem)((BindingSource)sender).Current).ID;
            medlemChanged = true;
        }

        void bs_CurrentItemChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            int curr = ((medlem) ((BindingSource) sender).Current).ID;
        }

        void bs_DataMemberChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            int curr = ((medlem)((BindingSource)sender).Current).ID;
        }

        private void UpdateRegistrerCheck()
        {
            int medlemId = ((medlem)bs.Current).ID;
            if (context.deltagers.Any(d => d.medlem_id == medlemId && d.auksjon_id == auksjonId))
            {
                chkRegistrer.CheckState = CheckState.Checked;
                erRegistrert = true;
            }
            else
            {
                chkRegistrer.CheckState = CheckState.Unchecked;
                erRegistrert = false;
            }
        }

        private void BsOnCurrentChanged(object sender, EventArgs eventArgs)
        {
            //throw new NotImplementedException();
            UpdateRegistrerCheck();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            bs.MoveFirst();
        }

        private void btn_previous_Click(object sender, EventArgs e)
        {
            bs.MovePrevious();
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            bs.MoveNext();
        }

        private void btn_end_Click(object sender, EventArgs e)
        {
            bs.MoveLast();
        }

        private void tb_current_KeyPress(object sender, KeyPressEventArgs e)
        {
                      if (e.KeyChar == (char)Keys.Return)
            {
                int inNummer = Int16.Parse(((TextBox)sender).Text);
                try
                {
                    var nyObj = context.medlems.Single(m => m.medlem_nummer == inNummer);
                    var index = context.medlems.ToList().IndexOf(nyObj);
                    bs.Position = index;
                }
                catch
                {
                }
                e.Handled = true;
                base.OnKeyPress(e);
            }
        }

        private void btnLagre_Click(object sender, EventArgs e)
        {
            bool registert = chkRegistrer.CheckState == CheckState.Checked;
            int medlemId = ((medlem) bs.Current).ID;
            IQueryable<deltager> regDeltagere =
                context.deltagers.Where(d => d.auksjon_id == auksjonId && d.medlem_id == medlemId);
            if (registert == false)
            {
                foreach (deltager delt in regDeltagere)
                {
                    context.deltagers.DeleteOnSubmit(delt);
                }
            }
            else
            {
                if (!regDeltagere.Any())
                {
                    var nyDelt = new deltager {auksjon_id = auksjonId, medlem_id = medlemId};
                    context.deltagers.InsertOnSubmit(nyDelt);
                }
            }
    
            context.SubmitChanges();
            medlemChanged = false;
        }

        private void btnAvslutt_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
