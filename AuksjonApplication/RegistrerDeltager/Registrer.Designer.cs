﻿namespace RegistrerDeltager
{
    partial class Registrer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_current = new System.Windows.Forms.TextBox();
            this.btn_end = new System.Windows.Forms.Button();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_previous = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.tbFornavn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMellomnavn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEtternavn = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPostnummer = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbGate2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbGate1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbPoststed = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbMaxGjenstander = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbKonto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbEpost = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbLand = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbMedlemNummer = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLagre = new System.Windows.Forms.Button();
            this.btnAvslutt = new System.Windows.Forms.Button();
            this.lblAuksjonNavn = new System.Windows.Forms.Label();
            this.tbPrioritet = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkRegistrer = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tb_current);
            this.panel1.Controls.Add(this.btn_end);
            this.panel1.Controls.Add(this.btn_next);
            this.panel1.Controls.Add(this.btn_previous);
            this.panel1.Controls.Add(this.btn_start);
            this.panel1.Location = new System.Drawing.Point(32, 516);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 35);
            this.panel1.TabIndex = 0;
            // 
            // tb_current
            // 
            this.tb_current.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tb_current.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_current.Location = new System.Drawing.Point(95, 5);
            this.tb_current.Name = "tb_current";
            this.tb_current.Size = new System.Drawing.Size(57, 25);
            this.tb_current.TabIndex = 1;
            this.tb_current.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_current_KeyPress);
            // 
            // btn_end
            // 
            this.btn_end.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_end.Location = new System.Drawing.Point(199, 4);
            this.btn_end.Name = "btn_end";
            this.btn_end.Size = new System.Drawing.Size(45, 27);
            this.btn_end.TabIndex = 4;
            this.btn_end.Text = ">|";
            this.btn_end.UseVisualStyleBackColor = true;
            this.btn_end.Click += new System.EventHandler(this.btn_end_Click);
            // 
            // btn_next
            // 
            this.btn_next.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_next.Location = new System.Drawing.Point(153, 4);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(45, 27);
            this.btn_next.TabIndex = 3;
            this.btn_next.Text = ">";
            this.btn_next.UseVisualStyleBackColor = true;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_previous
            // 
            this.btn_previous.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_previous.Location = new System.Drawing.Point(49, 4);
            this.btn_previous.Name = "btn_previous";
            this.btn_previous.Size = new System.Drawing.Size(45, 27);
            this.btn_previous.TabIndex = 2;
            this.btn_previous.Text = "<";
            this.btn_previous.UseVisualStyleBackColor = true;
            this.btn_previous.Click += new System.EventHandler(this.btn_previous_Click);
            // 
            // btn_start
            // 
            this.btn_start.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_start.Location = new System.Drawing.Point(3, 4);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(45, 27);
            this.btn_start.TabIndex = 1;
            this.btn_start.Text = "|<";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // tbFornavn
            // 
            this.tbFornavn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFornavn.Location = new System.Drawing.Point(112, 19);
            this.tbFornavn.Name = "tbFornavn";
            this.tbFornavn.Size = new System.Drawing.Size(149, 23);
            this.tbFornavn.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fornavn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(310, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Mellomnavn";
            // 
            // tbMellomnavn
            // 
            this.tbMellomnavn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMellomnavn.Location = new System.Drawing.Point(405, 19);
            this.tbMellomnavn.Name = "tbMellomnavn";
            this.tbMellomnavn.Size = new System.Drawing.Size(149, 23);
            this.tbMellomnavn.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Etternavn";
            // 
            // tbEtternavn
            // 
            this.tbEtternavn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEtternavn.Location = new System.Drawing.Point(112, 49);
            this.tbEtternavn.Name = "tbEtternavn";
            this.tbEtternavn.Size = new System.Drawing.Size(442, 23);
            this.tbEtternavn.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Postnummer";
            // 
            // tbPostnummer
            // 
            this.tbPostnummer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPostnummer.Location = new System.Drawing.Point(112, 81);
            this.tbPostnummer.Name = "tbPostnummer";
            this.tbPostnummer.Size = new System.Drawing.Size(149, 23);
            this.tbPostnummer.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 18);
            this.label6.TabIndex = 13;
            this.label6.Text = "Adresse2";
            // 
            // tbGate2
            // 
            this.tbGate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGate2.Location = new System.Drawing.Point(112, 51);
            this.tbGate2.Name = "tbGate2";
            this.tbGate2.Size = new System.Drawing.Size(442, 23);
            this.tbGate2.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 18);
            this.label7.TabIndex = 15;
            this.label7.Text = "Adresse1";
            // 
            // tbGate1
            // 
            this.tbGate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGate1.Location = new System.Drawing.Point(112, 21);
            this.tbGate1.Name = "tbGate1";
            this.tbGate1.Size = new System.Drawing.Size(442, 23);
            this.tbGate1.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(310, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 18);
            this.label8.TabIndex = 17;
            this.label8.Text = "Poststed";
            // 
            // tbPoststed
            // 
            this.tbPoststed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPoststed.Location = new System.Drawing.Point(405, 81);
            this.tbPoststed.Name = "tbPoststed";
            this.tbPoststed.Size = new System.Drawing.Size(149, 23);
            this.tbPoststed.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 18);
            this.label9.TabIndex = 19;
            this.label9.Text = "Max antall gjenstander";
            // 
            // tbMaxGjenstander
            // 
            this.tbMaxGjenstander.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMaxGjenstander.Location = new System.Drawing.Point(177, 48);
            this.tbMaxGjenstander.Name = "tbMaxGjenstander";
            this.tbMaxGjenstander.Size = new System.Drawing.Size(84, 23);
            this.tbMaxGjenstander.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Kontonr.";
            // 
            // tbKonto
            // 
            this.tbKonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKonto.Location = new System.Drawing.Point(112, 49);
            this.tbKonto.Name = "tbKonto";
            this.tbKonto.Size = new System.Drawing.Size(149, 23);
            this.tbKonto.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 18);
            this.label3.TabIndex = 23;
            this.label3.Text = "E-post";
            // 
            // tbEpost
            // 
            this.tbEpost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEpost.Location = new System.Drawing.Point(112, 20);
            this.tbEpost.Name = "tbEpost";
            this.tbEpost.Size = new System.Drawing.Size(442, 23);
            this.tbEpost.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(17, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 18);
            this.label11.TabIndex = 25;
            this.label11.Text = "Land";
            // 
            // tbLand
            // 
            this.tbLand.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLand.Location = new System.Drawing.Point(112, 111);
            this.tbLand.Name = "tbLand";
            this.tbLand.Size = new System.Drawing.Size(149, 23);
            this.tbLand.TabIndex = 24;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbMedlemNummer);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tbFornavn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbMellomnavn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbEtternavn);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(588, 114);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Navn";
            // 
            // tbMedlemNummer
            // 
            this.tbMedlemNummer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMedlemNummer.Location = new System.Drawing.Point(112, 80);
            this.tbMedlemNummer.Name = "tbMedlemNummer";
            this.tbMedlemNummer.Size = new System.Drawing.Size(149, 23);
            this.tbMedlemNummer.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 81);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 18);
            this.label12.TabIndex = 11;
            this.label12.Text = "Medlem Nr";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tbPostnummer);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbLand);
            this.groupBox2.Controls.Add(this.tbGate2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbGate1);
            this.groupBox2.Controls.Add(this.tbPoststed);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 181);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(588, 145);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adresse";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbEpost);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.tbKonto);
            this.groupBox3.Location = new System.Drawing.Point(12, 332);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(588, 83);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Annet";
            // 
            // btnLagre
            // 
            this.btnLagre.Location = new System.Drawing.Point(404, 516);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 36);
            this.btnLagre.TabIndex = 29;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            this.btnLagre.Click += new System.EventHandler(this.btnLagre_Click);
            // 
            // btnAvslutt
            // 
            this.btnAvslutt.Location = new System.Drawing.Point(491, 516);
            this.btnAvslutt.Name = "btnAvslutt";
            this.btnAvslutt.Size = new System.Drawing.Size(75, 36);
            this.btnAvslutt.TabIndex = 30;
            this.btnAvslutt.Text = "Avslutt";
            this.btnAvslutt.UseVisualStyleBackColor = true;
            this.btnAvslutt.Click += new System.EventHandler(this.btnAvslutt_Click);
            // 
            // lblAuksjonNavn
            // 
            this.lblAuksjonNavn.AutoSize = true;
            this.lblAuksjonNavn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuksjonNavn.Location = new System.Drawing.Point(16, 26);
            this.lblAuksjonNavn.Name = "lblAuksjonNavn";
            this.lblAuksjonNavn.Size = new System.Drawing.Size(83, 20);
            this.lblAuksjonNavn.TabIndex = 31;
            this.lblAuksjonNavn.Text = "AuksjonID";
            // 
            // tbPrioritet
            // 
            this.tbPrioritet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrioritet.Location = new System.Drawing.Point(177, 19);
            this.tbPrioritet.Name = "tbPrioritet";
            this.tbPrioritet.Size = new System.Drawing.Size(84, 23);
            this.tbPrioritet.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 18);
            this.label13.TabIndex = 26;
            this.label13.Text = "Utrop prioritet";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbPrioritet);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.chkRegistrer);
            this.groupBox4.Controls.Add(this.tbMaxGjenstander);
            this.groupBox4.Location = new System.Drawing.Point(12, 421);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(588, 80);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Auksjon";
            // 
            // chkRegistrer
            // 
            this.chkRegistrer.AutoSize = true;
            this.chkRegistrer.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRegistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRegistrer.Location = new System.Drawing.Point(473, 47);
            this.chkRegistrer.Name = "chkRegistrer";
            this.chkRegistrer.Size = new System.Drawing.Size(81, 22);
            this.chkRegistrer.TabIndex = 24;
            this.chkRegistrer.Text = "Påmeldt";
            this.chkRegistrer.UseVisualStyleBackColor = true;
            // 
            // Registrer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 563);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.lblAuksjonNavn);
            this.Controls.Add(this.btnAvslutt);
            this.Controls.Add(this.btnLagre);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Registrer";
            this.Text = "Registrer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_end;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_previous;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.TextBox tb_current;
        private System.Windows.Forms.TextBox tbFornavn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbMellomnavn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbEtternavn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPostnummer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbGate2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbGate1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPoststed;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbMaxGjenstander;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbKonto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbEpost;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbLand;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnLagre;
        private System.Windows.Forms.Button btnAvslutt;
        private System.Windows.Forms.Label lblAuksjonNavn;
        private System.Windows.Forms.TextBox tbMedlemNummer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbPrioritet;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chkRegistrer;
    }
}

